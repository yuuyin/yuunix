{ config
, lib
, pkgs
, ...
}:

{
  users = {
    groups.yin = { };

    users.yin = {
      isNormalUser = true;
      extraGroups = [
        "yin"
        "wheel" # for sudo
      ];
    };
  };
}
