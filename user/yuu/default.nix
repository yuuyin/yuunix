{ config
, pkgs
, pkgs-update
, ...
}:

let
  yuush = pkgs.mksh;
in {
  users = {
    users.yuu = {
      isNormalUser = true;
      createHome = true;
      shell = yuush;
      group = "yuu";
      extraGroups = [
        "adbusers" # for Android Debug Bridge
        "disk" # for accesing ro devices
        "lp" # for CUPS
        "scanner" # for SANE
        "users"
        "video"
        "wheel" # for sudo
        "libvirtd" # for virt-manager
      ];
      home = "/home/yuu";
    };

    groups.yuu = { };
  };

  home-manager.users.yuu = {
    home = {
      stateVersion = "22.05";
      username = "yuu";
      homeDirectory = "/home/yuu";
    };
  };

  imports = [
    ./packages.nix
  ];
  
  environment.shells = [
    "${yuush}/bin/mksh"
  ];
}
