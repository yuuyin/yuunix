{ config
, lib
, pkgs
, pkgs-update
, inputs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = (with pkgs; [
      rlwrap

      alacritty
      alacritty.terminfo
      neofetch
      tealdeer

      gramps

      srm

      # lighttable

      psmisc # for killall

      curl
      curlie

      nvfetcher # for getting latest version of packages and then overlay

      libarchive # for bsdtar
      zip
      p7zip
      borgbackup
      rsync
      rustic-rs
      # veracrypt

      parted
      woeusb
      exfatprogs
      ventoy-bin-full

      # hexchat # needs dnsmasq?

      rdrview
      libxml2 # for xmllint
      libseccomp
      pandoc
      # mdbook-epub

      llvmPackages.bintools-unwrapped

      gnumake

      exiftool

      patchelf

      pciutils # lspci

      # josm
      merkaartor
      marble

      apt
      pacman

      tz
      gnome.gnome-clocks

      remmina

      dolphin-emu-beta

      onboard

      janet

      # matrix rain
      tmatrix
      neo

      # CS theory
      jflap
      # https://github.com/flapjs/FLAPJS-WebApp

      # PRESENTATION
      nodePackages."reveal.js"

      ntfs3g

      # data recovery
      ddrescue
      testdisk
      safecopy

      # nix
      steam-run

      # math
      yacas
      gnuplot

      gnome.gucharmap

      whois

      # freecad

      # kicad
      # librepcb
      # geda
      # horizon-eda

      # argos-translate

      qrencode
      rustdesk
      inputs.gpt4all-nix.packages."x86_64-linux".default
    ]) ++ (with pkgs-update; [
      awscli2

      # https://github.com/NixOS/nixpkgs/pull/198154
      jami-client-qt
      jami-daemon

      element-desktop
      tdesktop

      ungoogled-chromium
      nyxt # depends on glib-network > networkmanager > dnsmasq
      w3m

      # bisq-desktop

      # proxy
      dante
      socat

      # tutanota-desktop

      # fritzing
      libngspice

      tuifeed

      # goldendict
    ]);
  };
}
