{ lib
, stdenv
, fetchFromGitHub
, leiningen
}:

let
  pname = "protoc-plugin";
  version = "2.1.0";
in stdenvNoCC.mkDerivation {
  inherit pname version;

  src = fetchFromGitHub {
    owner = "protojure";
    repo = pname;
    rev = "v${version}";
    sha256 = "1cap1vd9hp51bamb0nd3mkxvk3087j97ismg76c7f4av56w287hn";
  };

  nativeBuildInputs = [
    leiningen
  ];

  buildInputs = [
    protobuf
  ];

  meta = with lib; {
    description = "A protoc compiler plugin for Clojure applications";
    homepage = "https://github.com/protojure/protoc-plugin";
    license = licenses.CHANGE;
    # maintainers = with maintainers; [  ];
  };
}
