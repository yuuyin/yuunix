{ lib
, buildGoModule
, fetchFromGitHub
, git
}:

buildGoModule rec {
  pname = "src-cli";
  version = "5.0.2";

  src = fetchFromGitHub {
    owner = "sourcegraph";
    repo = "src-cli";
    rev = version;
    hash = "sha256-cPWP5NgNApS7PLWwbJtJOEsBZzDDG4QJPH98oNtIk3g=";
  };

  vendorHash = "sha256-NMLrBYGscZexnR43I4Ku9aqzJr38z2QAnZo0RouHFrc=";

  buildInputs = [
    git
  ];

  ldflags = [ 
    "-X" 
    "github.com/sourcegraph/src-cli/internal/version.BuildTag=${version}" 
  ];

  meta = with lib; {
    description = "Sourcegraph command line interface";
    homepage = "https://github.com/sourcegraph/src-cli";
    changelog = "https://github.com/sourcegraph/src-cli/blob/${src.rev}/CHANGELOG.md";
    license = licenses.asl20;
    maintainers = with maintainers; [ yuu ];
  };
}
