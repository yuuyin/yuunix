{ lib
, clangStdenv
, fetchFromGitHub
, cairo
, libX11
, libXfixes
, libXi
, libXinerama
, libXt
, xorgproto
, xorgserver
, withPicomSupport ? false
, picom
}:

let
  pname = "activate-linux";
  version = "0.0.1";
in clangStdenv.mkDerivation {
  inherit pname version;

  src = fetchFromGitHub {
    owner = "MrGlockenspiel";
    repo = pname;
    rev = "v${version}";
    sha256 = "0klzm8063vyay5m06c9zabfhzadgzkkv377rxhl838d60grndfrj";
  };

  postPatch = ''
    substituteInPlace Makefile \
      --replace "-I/usr/include/cairo" "-I${cairo.dev}/include/cairo" \
      --replace "-I/opt/local/include/cairo" "-I${cairo.dev}/include/cairo"
  '';

  buildInputs = [
    cairo.dev
    # xorg.<pname>
    libX11.dev
    libXfixes.dev
    libXi.dev
    libXinerama.dev
    libXt.dev
    xorgproto
  ] ++ lib.optionals withPicomSupport [
    picom
  ] ++ lib.optionals clangStdenv.isDarwin [
    cairo
    # xorg.<pname>
    libXinerama
    xorgserver
  ];

  installPhase = ''
    install -Dm755 bin/* -t $out/bin
  '';

  meta = with lib; {
    description = "The \"Activate Windows\" watermark ported to Linux and MacOS";
    homepage = "https://github.com/MrGlockenspiel/activate-linux";
    # license = licenses.unfreeRedistributable;
    license = licenses.free;
    maintainers = with maintainers; [
      yuu
    ];
    platforms = with platforms; linux ++ darwin;
  };
}
