{ lib
, stdenv
, python3
, fetchFromGitHub
, substituteInPlace
, cmake
, cxxopts
, dnnl
, mkl
, openblas
, openmp
, spdlog
, thrust
, cudnn ? null
, withCudnn ? false
}:

let
  thrust = (
    stdenv.mkDerivation rec {
      pname = "nvidia-thrust";
      version = "2.0.1";

      src = fetchFromGitHub {
        owner = "nvidia";
        repo = "thrust";
        rev = version;
        hash = "sha256-Fvvbg6HFD1D1HHKRQopnHdwm8Rr7LPYtD+fgNEjuBAs=";
      };

      nativeBuildInputs = [
        cmake
      ];

      meta = with lib; {
        description = "C++ parallel algorithms library";
        homepage = "https://github.com/nvidia/thrust";
        changelog = "https://github.com/nvidia/thrust/blob/${src.rev}/CHANGELOG.md";
        license = licenses.asl20;
        maintainers = with maintainers; [ yuu ];
      };
    }
  );
  ctranslate2 = (
    stdenv.mkDerivation rec {
      pname = "ctranslate2";
      version = "3.8.0";

      src = fetchFromGitHub {
        owner = "opennmt";
        repo = "ctranslate2";
        rev = "v${version}";
        hash = "sha256-QDjmENw6VZtMbN9KKlyFtCochRPlRS8b4a6eVQXAGOM=";
      };

      postPatch = ''
        substituteInPlace CMakeLists.txt \
          --replace "${CMAKE_CURRENT_SOURCE_DIR}/third_party/thrust" "${thrust}" \
          --replace "${CMAKE_CURRENT_SOURCE_DIR}/third_party/thrust/dependencies/cub" "${thrust}/"
      '';

      nativeBuildInputs = [
        cmake
      ];

      buildInputs = [
        cxxopts
        dnnl
        mkl
        openblas
        openmp
        spdlog
        thrust
      ] ++ lib.optional withCudnn cudnn;

      meta = with lib; {
        description = "Fast inference engine for Transformer models";
        homepage = "https://github.com/opennmt/ctranslate2";
        changelog = "https://github.com/opennmt/ctranslate2/blob/${src.rev}/CHANGELOG.md";
        license = licenses.mit;
        maintainers = with maintainers; [
          yuu
        ];
      };
    }
  );
in python3.pkgs.buildPythonApplication rec {
  pname = "argos-translate";
  version = "unstable-2023-03-03";
  format = "setuptools";

  src = fetchFromGitHub {
    owner = "argosopentech";
    repo = "argos-translate";
    rev = "23be8352e0ba38d01b901fa4b32d96a3fea196e0";
    hash = "sha256-QQdr7ZD+WocPxDHbVX3ue80QPNI4mmotNXqE+Fhc3zY=";
  };

  buildInputs = [
    ctranslate2
  ];

  propagatedBuildInputs = with python3.pkgs; [
    sentencepiece
    stanza
  ];

  pythonImportsCheck = [
    "argos-translate"
  ];

  meta = with lib; {
    description = "Offline translation Python library and command line tool";
    homepage = "https://github.com/argosopentech/argos-translate";
    license = licenses.mit;
    maintainers = with maintainers; [
      yuu
    ];
  };
}
