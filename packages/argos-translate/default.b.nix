{ lib
, python3
, ctranslate2
, fetchFromGitHub
}:

python3.pkgs.buildPythonApplication rec {
  pname = "argos-translate";
  version = "unstable-2023-03-03";
  format = "setuptools";

  src = fetchFromGitHub {
    owner = "argosopentech";
    repo = "argos-translate";
    rev = "23be8352e0ba38d01b901fa4b32d96a3fea196e0";
    hash = "sha256-QQdr7ZD+WocPxDHbVX3ue80QPNI4mmotNXqE+Fhc3zY=";
  };

  buildInputs = [
    ctranslate2
  ];

  propagatedBuildInputs = with python3.pkgs; [
    sentencepiece
    stanza
  ];

  pythonImportsCheck = [
    "argos-translate"
  ];

  meta = with lib; {
    description = "Offline translation Python library and command line tool";
    homepage = "https://github.com/argosopentech/argos-translate";
    license = licenses.mit;
    maintainers = with maintainers; [ yuu ];
  };
}
