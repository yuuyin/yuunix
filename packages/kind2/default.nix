{ lib
, rustPlatform
, fetchFromGitHub
}:

let
  pname = "kind2";
  version = "unstable-2022-12-02";
in rustPlatform.buildRustPackage {
  inherit pname version;

  src = fetchFromGitHub {
    owner = "kindelia";
    repo = pname;
    rev = "ec94c208d463aff83269b25097abb1df860afbae";
    hash = "sha256-5A1KZ1cujj30OLSJO5h9ONivCYAboiXU3iWOJM5AEz0=";
  };

  cargoSha256 = lib.fakeHash;

  meta = with lib; {
    description = "A next-gen functional language";
    homepage = "https://github.com/kindelia/kind2";
    license = licenses.mit;
    maintainers = with maintainers; [
      yuu
    ];
  };
}
