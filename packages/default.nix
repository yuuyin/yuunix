## flake.nix

# Copyright (C) 2022--2023  Yuu Yin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Commentary

## Code

{ pkgs ? null
, pkgs-update ? null
}:

{
  # activate-linux = pkgs.callPackage ./activate-linux { };

  # ctranslate2 = pkgs.callPackage ./ctranslate2 {
  #   openmp = pkgs.llvmPackages_15.openmp;
  #   cudnn = pkgs.cudaPackages.cudnn;
  # };

  # argos-translate = pkgs.callPackage ./argos-translate {
  #   # FIXME: ctranslate2
  #   # ctranslate2 = (pkgs.callPackage ./ctranslate2);
  #   openmp = pkgs.llvmPackages_15.openmp;
  #   cudnn = pkgs.cudaPackages.cudnn;
  # };

  # clojure-protoc-plugin = pkgs.callPackage ./clojure-protoc-plugin { };

  # mdbook-epub = pkgs.callPackage ./mdbook-epub { };

  # kind2 = pkgs.callPackage ./kind2 { };

  # simplex-chat = inputs.simplex-chat.packages."exe:simplex-chat.x86_64-linux";

  src-cli = pkgs.callPackage ./src-cli { };

  yarn-berry = pkgs.callPackage ./yarn-berry { };
}
