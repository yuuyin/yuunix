{ lib
, stdenv
, fetchFromGitHub
, cmake
, cxxopts
, dnnl
, mkl
, openblas
, openmp
, spdlog
, thrust
, cudnn ? null
, withCudnn ? false
}:

stdenv.mkDerivation rec {
  pname = "ctranslate2";
  version = "3.8.0";

  src = fetchFromGitHub {
    owner = "opennmt";
    repo = "ctranslate2";
    rev = "v${version}";
    hash = "sha256-QDjmENw6VZtMbN9KKlyFtCochRPlRS8b4a6eVQXAGOM=";
  };

  nativeBuildInputs = [
    cmake
  ];

  buildInputs = [
    cxxopts
    dnnl
    mkl
    openblas
    openmp
    spdlog
    thrust
  ] ++ lib.optional withCudnn cudnn;

  meta = with lib; {
    description = "Fast inference engine for Transformer models";
    homepage = "https://github.com/opennmt/ctranslate2";
    changelog = "https://github.com/opennmt/ctranslate2/blob/${src.rev}/CHANGELOG.md";
    license = licenses.mit;
    maintainers = with maintainers; [
      yuu
    ];
  };
}
