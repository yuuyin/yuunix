## flake.nix

# Copyright (C) 2022--2023  Yuu Yin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

## COMMENTARY

# See `man 1 nix3-flake'

## CODE

{
  description = "Yuunix: Yuu's Nix[OS] system";

  # See `man 5 nix.conf'
  nixConfig = {
    extra-substituters = [
      # "https://emacsng.cachix.org"
      "https://foo-dogsquared.cachix.org"
      "https://helix.cachix.org"
      "https://nickel.cachix.org"
      "https://nix-community.cachix.org"
      "https://nixpkgs-wayland.cachix.org"
    ];

    extra-trusted-public-keys = [
      # "emacsng.cachix.org-1:i7wOr4YpdRpWWtShI8bT6V7lOTnPeI7Ho6HaZegFWMI="
      "foo-dogsquared.cachix.org-1:/2fmqn/gLGvCs5EDeQmqwtus02TUmGy0ZlAEXqRE70E="
      "helix.cachix.org-1:ejp9KQpR1FBI2onstMQ34yogDm4OgU2ru6lIwPvuCVs="
      "nickel.cachix.org-1:ABoCOGpTJbAum7U6c+04VbjvLxG9f0gJP5kYihRRdQs="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "nixpkgs-wayland.cachix.org-1:3lwxaILxMRkVhehr5StQprHdEo4IrE8sRho9R9HOLYA="
    ];
  };

  # Specifies other flakes which this flake depends on.
  inputs = {
    ### NIXPKGS

    nixpkgs = {
      type = "github";
      owner = "nixos";
      repo = "nixpkgs";
      rev = "76e2e65e39a3760582cca07b848f5719590190a7";
    };

    # For apps needing faster updates apps (e.g. security critical)
    # such as web-engine based (web browsers, electron), etc. Check
    # https://hydra.nixos.org/job/nixos/trunk-combined/nixos.tests.firefox.x86_64-linux
    # https://hydra.nixos.org/job/nixos/trunk-combined/nixpkgs.element-desktop.x86_64-linux
    nixpkgs-update = {
      type = "github";
      owner = "nixos";
      repo = "nixpkgs";
      ref = "nixos-unstable";
      rev = "82a315a053b04583309639dd5a1870e24f54bde6";
    };

    nixpkgs-master = {
      type = "github";
      owner = "nixos";
      repo = "nixpkgs";
      ref = "master";
      rev = "82a315a053b04583309639dd5a1870e24f54bde6";
    };

    ##### NIXPKGS+

    nur = {
      type = "github";
      owner = "nix-community";
      repo = "nur";
    };

    ### NIX LIBRARIES/TOOLS

    nixckel = {
      type = "github";
      owner = "tweag";
      repo = "nickel";
    };

    flake-utils = {
      type = "github";
      owner = "numtide";
      repo = "flake-utils";
    };

    devshell = {
      type = "github";
      owner = "numtide";
      repo = "devshell";
    };

    ### 2NIX

    # cargo2nix = {
    #   type = "github";
    #   owner = "cargo2nix";
    #   repo = "cargo2nix";
    #   ref = "unstable";
    # };

    dream2nix = {
      type = "github";
      owner = "nix-community";
      repo = "dream2nix";
    };

    clj-nix = {
      type = "github";
      owner = "jlesquembre";
      repo = "clj-nix";
    };

    poetry2nix = {
      type = "github";
      owner = "nix-community";
      repo = "poetry2nix";
      # no flake.lock
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # yarnpnp2nix = {
    #   type = "github";
    #   owner = "madjam002";
    #   repo = "yarnpnp2nix";
    # };

    ### NIXPKGS TOOLS

    nixpkgs-hammering.url = "github:jtojnar/nixpkgs-hammering";

    nix-init = {
      type = "github";
      owner = "nix-community";
      repo = "nix-init";
    };

    ### NIX SYSTEM

    home-manager = {
      type = "github";
      owner = "nix-community";
      repo = "home-manager";
    };

    ### EMACS

    # For changing the commit hash, see:
    # https://github.com/nix-community/emacs-overlay/issues/122#issuecomment-1016778377
    # https://github.com/nix-community/emacs-overlay/issues/122#issuecomment-1193081483
    # https://hydra.nix-community.org/job/emacs-overlay/unstable/emacsGit
    # NOTE: emacs-overlay and nixpkgs revisions
    # need to be the same as those used by Hydra
    # as to hit nix-community substituter/cache.
    emacs-overlay = {
      type = "github";
      owner = "nix-community";
      repo = "emacs-overlay";
      rev = "21d40b527260498eeb9aed8d2200e4f79d6b152c";
      # no flake.lock, but doesn't work anyway for hitting cache.
      inputs.nixpkgs = {
        type = "github";
        owner = "nixos";
        repo = "nixpkgs";
        rev = "3e313808bd2e0a0669430787fb22e43b2f4bf8bf";
      };
    };

    ### XMONAD

    xmonad = {
      type = "github";
      owner = "xmonad";
      repo = "xmonad";
      ref = "master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    xmonad-contrib = {
      type = "github";
      owner = "xmonad";
      repo = "xmonad-contrib";
      ref = "master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    ### WAYLAND

    nixpkgs-wayland = {
      type = "github";
      owner = "nix-community";
      repo = "nixpkgs-wayland";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # https://wiki.hyprland.org/Nix/
    hypr-land = {
      type = "github";
      owner = "hyprwm";
      repo = "hyprland";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    hypr-contrib = {
      type = "github";
      owner = "hyprwm";
      repo = "contrib";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    ### MISC
    helix = {
      type = "github";
      owner = "helix-editor";
      repo = "helix";
      ref = "master";
      # rev = "5a3ff742218aac32c3af08993f0edb623631fc72";
      # inputs.nixpkgs.follows = "nixpkgs";
    };

    kmonad = {
      type = "github";
      owner = "kmonad";
      repo = "kmonad";
      dir = "nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-overlay-guix = {
      type = "github";
      owner = "foo-dogsquared";
      repo = "nix-overlay-guix";
      rev = "6ea748a541fa994058de94903fd1adaa3e1f6a8a";
    };

    simplex-chat = {
      type = "github";
      owner = "simplex-chat";
      repo = "simplex-chat";
      ref = "master";
    };

    sops-nix = {
      type = "github";
      owner = "mic92";
      repo = "sops-nix";
      ref = "master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Kindelia
    # hvm = {
    #   type = "github";
    #   owner = "kindelia";
    #   repo = "hvm";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };

    scientific-fhs = {
      type = "github";
      owner = "olynch";
      repo = "scientific-fhs";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    gpt4all-nix = {
      type = "github";
      owner = "polygon";
      repo = "gpt4all-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  # Function that produces an attribute set.
  # Its function arguments are the flakes specified in inputs.
  # The self argument denotes this flake.
  outputs = (inputs@{
    self
    , nixpkgs
    , nixpkgs-update
    , clj-nix
    , emacs-overlay
    , gpt4all-nix
    , helix
    , home-manager
    , kmonad
    , nickel
    , nix-overlay-guix
    , nixpkgs-hammering
    , nixpkgs-wayland
    , nur
    , poetry2nix
    , simplex-chat
    , sops-nix
    , xmonad
    , xmonad-contrib
    , ...
  }:
    let
      inherit (nixpkgs.lib) filterAttrs;
      inherit (builtins) mapAttrs elem;
      inherit (self) outputs;

      # OLD
      # "legacy" because packages is expected to be flat while nixpkgs is not.
      # pkgs = nixpkgs.legacyPackages.${system};
      # pkgs-update = nixpkgs-update.legacyPackages.${system};
      lib = import nixpkgs.lib;

      supportedLocalSystems = [
        "x86_64-linux"
        "aarch64-linux"
      ];
      # Partial application.
      forAllLocalSystems = nixpkgs.lib.genAttrs supportedLocalSystems;
      liLocalSystem = "x86_64-linux";

      templates = import ./templates;
      nixosModules = import ./modules/nixos;
      homeManagerModules = import ./modules/home-manager;
      overlays = import ./overlays;

      legacyPackagesUpdate = forAllLocalSystems (localSystem:
        import nixpkgs-update {
          inherit localSystem;

          overlays = (builtins.attrValues outputs.overlays);
          config.allowUnfree = true;
        }
      );
    in {
      inherit
        forAllLocalSystems
        legacyPackagesUpdate
        liLocalSystem
        templates
        nixosModules
        homeManagerModules
        overlays
      ;

      packages = forAllLocalSystems (localSystem:
        # Import my packages, some use pkgs, others pkgs-update.
        import ./packages {
          pkgs = nixpkgs.legacyPackages.${localSystem};
          pkgs-update = legacyPackagesUpdate.${localSystem};
        }
      );

      devShells = forAllLocalSystems (localSystem: {
        default = nixpkgs.legacyPackages.${localSystem}.callPackage ./shell.nix {
          pkgs = nixpkgs.legacyPackages.${localSystem};
        };
      });

      # Attribute set which nixos-rebuild looks for.
      nixosConfigurations = {
        li = nixpkgs.lib.nixosSystem {
          specialArgs = {
            inherit
              inputs
              outputs
            ;

            # pkgs-update = nixpkgs-update.legacyPackages."${liLocalSystem}";
            pkgs-update = legacyPackagesUpdate."${liLocalSystem}";
          };

          modules = [
            ./host/li
          ];
        };
      };
    }
  );
}
