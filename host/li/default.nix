## li/default.nix

#
# Copyright (C) 2022  Yuu Yin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#
## Commentary

# Edit this configuration file to define what should be installed on your
# system. Help is available in the configuration.nix(5) man page and in the
# NixOS manual (accessible by running ‘nixos-help’).

#
## Code

{ lib
, pkgs
, inputs
, outputs
, ...
}:

{
  imports = [
    {
      nixpkgs = {
        overlays =
          (builtins.attrValues outputs.overlays);
        config = {
          allowUnfree = true;
        };
      };
    }
  ] ++ [
    ./hardware-configuration.nix

    ../../profile/app/android.nix
    # ../../profile/app/aria2.nix
    ../../profile/app/astronomy.nix
    ../../profile/app/container.nix
    ../../profile/app/copyq.nix
    # ../../profile/app/fcitx.nix
    ../../profile/app/firefox.nix
    ../../profile/app/flatpak.nix
    ../../profile/app/guix.nix
    ../../profile/app/helix.nix
    ../../profile/app/imv.nix
    ../../profile/app/jq.nix
    ../../profile/app/kdeconnect.nix
    #../../profile/app/kmonad.nix
    ../../profile/app/key.nix
    ../../profile/app/lf.nix
    ../../profile/app/locate.nix
    ../../profile/app/mosh.nix
    ../../profile/app/neovim.nix
    ../../profile/app/nushell.nix
    # ../../profile/app/ps2.nix
    ../../profile/app/tmux.nix
    ../../profile/app/virt.nix
    ../../profile/app/vscod.nix
    ../../profile/app/xdg.nix
    # ../../profile/app/zsh.nix

    ../../profile/gui/backlight.nix
    ../../profile/gui/dunst.nix
    ../../profile/gui/eww.nix
    ../../profile/gui/fonts.nix
    ../../profile/gui/gtk.nix
    ../../profile/gui/icon.nix
    ../../profile/gui/notification.nix
    ../../profile/gui/polybar.nix
    ../../profile/gui/qt.nix
    ../../profile/gui/river.nix
    ../../profile/gui/rofi.nix
    ../../profile/gui/screenlock.nix
    ../../profile/gui/wayland/sway.nix
    ../../profile/gui/x.nix
    ../../profile/gui/xbindkeys.nix
    ../../profile/gui/xmonad.nix

    ../../profile/dev/agda.nix
    ../../profile/dev/android.nix
    ../../profile/dev/asm.nix
    ../../profile/dev/c.nix
    # ../../profile/dev/chez.nix
    ../../profile/dev/clojure.nix
    ../../profile/dev/common-lisp.nix
    ../../profile/dev/configuration-management.nix
    # ../../profile/dev/dart.nix
    ../../profile/dev/database.nix
    ../../profile/dev/devops.nix
    ../../profile/dev/dhall.nix
    ../../profile/dev/documentation.nix
    # ../../profile/dev/elixir.nix
    # ../../profile/dev/elm.nix
    # ../../profile/dev/erlang.nix
    # ../../profile/dev/f-sharp.nix
    # ../../profile/dev/fennel.nix
    ../../profile/dev/game.nix
    ../../profile/dev/grafana.nix
    ../../profile/dev/grpc.nix
    ../../profile/dev/guile.nix
    ../../profile/dev/haskell.nix
    ../../profile/dev/html-css.nix
    ../../profile/dev/java.nix
    ../../profile/dev/javascript.nix
    ../../profile/dev/julia.nix
    # ../../profile/dev/jupyter.nix
    # ../../profile/dev/kind.nix
    ../../profile/dev/kotlin.nix
    # ../../profile/dev/lean.nix
    ../../profile/dev/learn.nix
    ../../profile/dev/lua.nix
    ../../profile/dev/markup.nix
    ../../profile/dev/misc.nix
    ../../profile/dev/modeling.nix
    ../../profile/dev/nix.nix
    ../../profile/dev/perl.nix
    # ../../profile/dev/php.nix
    ../../profile/dev/python.nix
    ../../profile/dev/r.nix
    # ../../profile/dev/rabbitmq.nix
    # ../../profile/dev/racket.nix
    # ../../profile/dev/reason.nix
    # ../../profile/dev/redmine.nix
    ../../profile/dev/rust.nix
    # ../../profile/dev/scala.nix
    ../../profile/dev/security.nix
    ../../profile/dev/shell.nix
    # ../../profile/dev/shen.nix

    ../../profile/media/audio.nix
    ../../profile/media/graphics.nix
    ../../profile/media/multimedia.nix

    ../../profile/pro/archive.nix
    ../../profile/pro/document.nix
    ../../profile/pro/edu.nix
    ../../profile/pro/emacs.nix
    ../../profile/pro/email.nix
    ../../profile/pro/ocr.nix
    ../../profile/pro/pim.nix
    ../../profile/pro/reference.nix
    ../../profile/pro/tex.nix
    ../../profile/pro/typst.nix

    ../../profile/packages.nix

    ../../profile/sys/boot.nix
    ../../profile/sys/environment.nix
    # ../../profile/sys/gui.nix
    ../../profile/sys/locale.nix
    ../../profile/sys/memory.nix
    ../../profile/sys/power.nix
    ../../profile/sys/nix.nix
    ../../profile/sys/prime.nix
    ../../profile/sys/security.nix
    ../../profile/sys/ssd.nix
    ../../profile/sys/system.nix
    ../../profile/sys/swap.nix

    ../../user/yuu/default.nix
    ../../user/yin/default.nix
  ] ++ import ./_ ++[
    inputs.home-manager.nixosModules.home-manager
    # nur.nixosModules.nur
    # sops-nix.nixosModules.sops
    {
      home-manager = {
        extraSpecialArgs = {
          inherit
            inputs;
        };
        useUserPackages = true;
        useGlobalPkgs = true;
      };
    }
  ];
}
