-- -*- lsp-lens-mode: nil; -*-

{-# LANGUAGE FlexibleInstances #-}


import           Control.Monad (when)
import qualified Data.Map as M
import           XMonad -- (readsLayout)
import qualified XMonad.Actions.Contexts as Contexts
import           XMonad.Actions.CopyWindow
import           XMonad.Actions.DynamicWorkspaceGroups
import           XMonad.Actions.TopicSpace
import           XMonad.Config.Desktop
import           XMonad.Hooks.EwmhDesktops
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.ManageHelpers (doCenterFloat, doFullFloat, isDialog, isFullscreen)
import           XMonad.Hooks.RefocusLast (toggleFocus)
import           XMonad.Layout.Accordion
import           XMonad.Layout.Dishes
import           XMonad.Layout.Magnifier
import           XMonad.Layout.MultiToggle (EOT (EOT), mkToggle, single, (??))
import           XMonad.Layout.MultiToggle.Instances (StdTransformers (MIRROR, NBFULL, NOBORDERS))
import           XMonad.Layout.NoBorders
import           XMonad.Layout.PerWorkspace (onWorkspace, onWorkspaces)
import           XMonad.Layout.Tabbed
import           XMonad.Layout.TrackFloating (trackFloating, useTransientFor)
import           XMonad.Layout.ThreeColumns
import           XMonad.Layout.ToggleLayouts
import           XMonad.Prompt
import           XMonad.Prompt.Window
import qualified XMonad.StackSet as StackSet
import           XMonad.Util.Cursor
import qualified XMonad.Util.Dmenu as Dmenu
import           XMonad.Util.EZConfig (additionalKeys, additionalKeysP)
import           XMonad.Util.NamedScratchpad
import           XMonad.Util.Ungrab

--------------------------------------------------------------------------------

data Solarized = Solarized {
  base03    :: String
  , base02  :: String
  , base01  :: String
  , base00  :: String
  , base0   :: String
  , base1   :: String
  , base2   :: String
  , base3   :: String
  , yellow  :: String
  , orange  :: String
  , red     :: String
  , magenta :: String
  , violet  :: String
  , blue    :: String
  , cyan    :: String
  , green   :: String
}

solarized = Solarized {
  base03    = "#002b36"
  , base02  = "#073642"
  , base01  = "#586e75"
  , base00  = "#657b83"
  , base0   = "#839496"
  , base1   = "#93a1a1"
  , base2   = "#eee8d5"
  , base3   = "#fdf6e3"
  , yellow  = "#b58900"
  , orange  = "#cb4b16"
  , red     = "#dc322f"
  , magenta = "#d33682"
  , violet  = "#6c71c4"
  , blue    = "#268bd2"
  , cyan    = "#2aa198"
  , green   = "#859900"
  }

--------------------------------------------------------------------------------

myTerminal :: String
myTerminal = "alacritty"
-- myTerminal = "nvidia-offload alacritty"

myFont :: String
myFont = "xft:Iosevka:size=9:bold:antialias=true"

myTabTheme = def
  { fontName            = myFont
  , activeColor         = magenta solarized
  , inactiveColor       = base3 solarized
  , activeBorderColor   = magenta solarized
  , inactiveBorderColor = base3 solarized
  , activeTextColor     = base3 solarized
  , inactiveTextColor   = base03 solarized
  }

--------------------------------------------------------------------------------

myStartupHook = do
  setDefaultCursor xC_left_ptr

--------------------------------------------------------------------------------

myManageHook :: ManageHook
myManageHook =
  composeAll . concat $
    [
      [className =? c --> doCenterFloat | c <- myClassFloats]
    , [title =? t --> doFloat | t <- myTitleFloats]
    , [resource =? r --> doFloat | r <- myResourceFloats]
    , [resource =? i --> doIgnore | i <- myIgnores]
    ]
    where
      myClassFloats = [
        "arandr"
        , "copyq"
        , "fcitx5-config-qt"
        , "Galculator"
        , "Gcolor2"
        , "Gcolor3"
        , "Gnome-system-monitor"
        , "GParted"
        , "kdeconnect"
        , "Kvantum Manager"
        , "Lxappearance"
        , "mpv"
        , "Nitrogen"
        , "obs"
        , "octopi"
        , "org.gnome.Characters"
        , "Org.gnome.clocks"
        , "Pamac-manager"
        , "Pavucontrol"
        , "PCSX2"
        , "qjackctl"
        , "qt5ct"
        , "Qtconfig-qt4"
        , "Simple-scan"
        , "Skype"
        , "speedcrunch"
        , "sun-awt-X11-XFramePeer"
        , "System-config-printer.py"
        , "Timeset-gui"
        , "vmpk"
        , "VMPK-Virtual MIDI Piano Keyboard"
        , "Xfburn"
        , "Zotero"
        -- , "principal-Aplicacao" -- brModelo
        ]
      myTitleFloats = [
        "File Transfer"
        , "Firefox - Choose User Profile"
        , "Library" -- Firefox Places
        , "Helm"
        , "MuseScore: Play Panel"
        , "Event Tester"
        , "XBindKey"
        ]
      myResourceFloats = []
      myIgnores = []

--------------------------------------------------------------------------------

myExtraWorkspaces = [ (xK_0, "10"), (xK_minus, "11"), (xK_equal, "12"), (xK_BackSpace, "NSP") ]
myWorkspaces :: [[Char]]
myWorkspaces = ["1", "2","3","4","5","6","7","8","9"] ++ map snd myExtraWorkspaces

--------------------------------------------------------------------------------

myLayoutHook = avoidStruts $ smartBorders $ trackFloating $
  mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
  where
    myDefaultLayout =
      tabbed shrinkText myTabTheme
        ||| tiled
        ||| Mirror tiled
        ||| threeCol
        ||| Accordion
        ||| dishes
    tiled = Tall nMaster delta ratio
    threeCol = magnifiercz' 1.3 $ ThreeColMid nMaster delta ratio
    dishes = Dishes dMaster dRatio
    nMaster = 1 -- Default number of windows in the master pane
    ratio = 1 / 2 -- Default proportion of screen occupied by master pane
    delta = 3 / 100 -- Percent of screen to increment by when resizing panes
    dMaster = 2 -- Default number of windows in the master pane
    dRatio = 1 / 6
--        ||| noBorders Full


instance Read (Layout Window) where
    readsPrec _ = readsLayout (Layout myLayoutHook)

--------------------------------------------------------------------------------

myScratchpads :: [NamedScratchpad]
myScratchpads = [
  NS "emacs-float" spawnEmacsFloat findEmacsFloat manageEmacsFloat
  , NS "emacs-float-calculator" spawnEmacsFloatCalculator findEmacsFloatCalculator manageEmacsFloatCalculator
  , NS "emacs-float-restclient" spawnEmacsFloatRestClient findEmacsFloatRestClient manageEmacsFloatRestClient
  , NS "emacs-float-eshell" spawnEmacsFloatEshell findEmacsFloatEshell manageEmacsFloatEshell
  , NS "emacs-float-ielm" spawnEmacsFloatIelm findEmacsFloatIelm manageEmacsFloatIelm
  , NS "emacs-float-sly" spawnEmacsFloatSly findEmacsFloatSly manageEmacsFloatSly
  , NS "emacs-float-gpt" spawnEmacsFloatGpt findEmacsFloatGpt manageEmacsFloatGpt
  , NS "terminal-float" spawnTerminalFloat findTerminalFloat manageTerminalFloat
  , NS "mpv" spawnMPV findMPV manageMPV
  , NS "zotero" spawnZotero findZotero manageZotero
  , NS "copyq" spawnCopyQ findCopyQ manageCopyQ
  , NS "gcolor" spawnGcolor findGcolor manageGcolor
  , NS "lf" spawnLf findLf manageLf
  , NS "gnome-clocks" spawnGnomeClocks findGnomeClocks manageGnomeClocks
  , NS "obs" spawnObs findObs manageObs
  , NS "nyx" spawnNyx findNyx manageNyx
  , NS "gnome-system-monitor" spawnGnomeSystemMonitor findGnomeSystemMonitor manageGnomeSystemMonitor
  , NS "pulsemixer" spawnPulsemixer findPulsemixer managePulsemixer
  ]
  where
    -- spawnEmacsFloat = "emacsclient --create-frame --frame-parameters='((name . \"emacs-float\"))' --eval '(progn (switch-to-buffer (get-buffer-create \"*scratch*\")))' --socket-name=vanilla"
    spawnEmacsFloat = "emacsclient --create-frame --frame-parameters='((name . \"emacs-float\"))' --eval '(progn (switch-to-buffer (get-buffer-create \"*scratch*\")))'"
    findEmacsFloat = title =? "emacs-float"
    manageEmacsFloat = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.0
        width = 0.98
        height = 0.4
    -- spawnEmacsFloatCalculator = "emacsclient --create-frame --frame-parameters='((name . \"emacs-float-calculator\"))' --eval  '(progn (calc) (delete-other-windows))' --socket-name=vanilla"
    spawnEmacsFloatCalculator = "emacsclient --create-frame --frame-parameters='((name . \"emacs-float-calculator\"))' --eval  '(progn (calc) (delete-other-windows))'"
    findEmacsFloatCalculator = title =? "emacs-float-calculator"
    manageEmacsFloatCalculator = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.0
        width = 0.98
        height = 0.4
    spawnEmacsFloatRestClient = "emacsclient --create-frame --frame-parameters='((name . \"emacs-float-restclient\"))' --eval '(progn (switch-to-buffer (get-buffer-create \"emacs-restclient\") (restclient-mode)) (delete-other-windows))'"
    findEmacsFloatRestClient = title =? "emacs-float-restclient"
    manageEmacsFloatRestClient = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.0
        width = 0.98
        height = 0.4
    -- spawnEmacsFloatEshell = "emacsclient --create-frame --frame-parameters='((name . \"emacs-float-eshell\"))' --eval  '(progn (eshell) (delete-other-windows))' --socket-name=vanilla"
    spawnEmacsFloatEshell = "emacsclient --create-frame --frame-parameters='((name . \"emacs-float-eshell\"))' --eval  '(progn (eshell) (delete-other-windows))'"
    findEmacsFloatEshell = title =? "emacs-float-eshell"
    manageEmacsFloatEshell = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.0
        width = 0.98
        height = 0.4
    -- spawnEmacsFloatIelm = "emacsclient --create-frame --frame-parameters='((name . \"emacs-float-ielm\"))' --eval  '(progn (ielm) (delete-other-windows))' --socket-name=vanilla"
    spawnEmacsFloatIelm = "emacsclient --create-frame --frame-parameters='((name . \"emacs-float-ielm\"))' --eval  '(progn (ielm) (delete-other-windows))'"
    findEmacsFloatIelm = title =? "emacs-float-ielm"
    manageEmacsFloatIelm = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.0
        width = 0.98
        height = 0.4
    -- spawnEmacsFloatSly = "emacsclient --create-frame --frame-parameters='((name . \"emacs-float-sly\"))' --eval  '(progn (sly) (delete-other-windows))' --socket-name=vanilla"
    spawnEmacsFloatSly = "emacsclient --create-frame --frame-parameters='((name . \"emacs-float-sly\"))' --eval  '(progn (sly) (delete-other-windows))'"
    findEmacsFloatSly = title =? "emacs-float-sly"
    manageEmacsFloatSly = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.0
        width = 0.98
        height = 0.4
    spawnEmacsFloatGpt = "emacsclient --create-frame --frame-parameters='((name . \"emacs-float-gpt\"))' --eval  '(progn (call-interactively (quote gptel)) (delete-other-windows))'"
    findEmacsFloatGpt = title =? "emacs-float-gpt"
    manageEmacsFloatGpt = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.0
        width = 0.98
        height = 0.4
    spawnTerminalFloat = myTerminal ++ " --title terminal-float"
    findTerminalFloat = title=? "terminal-float"
    manageTerminalFloat = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.0
        width = 0.98
        height = 0.4
    spawnGnomeClocks = "gnome-clocks"
    findGnomeClocks = className =? "org.gnome.clocks"
    manageGnomeClocks = defaultFloating
    spawnObs = "obs"
    findObs = className =? "obs"
    manageObs = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.20
        width = 0.98
        height = 0.80
    spawnZotero = "zotero"
    findZotero = className =? "Zotero"
    manageZotero = defaultFloating
    spawnCopyQ = "copyq"
    findCopyQ = className =? "copyq"
    manageCopyQ = defaultFloating
    spawnGcolor = "gcolor2"
    findGcolor = className =? "Gcolor2"
    manageGcolor = defaultFloating
    spawnMPV = "mpv"
    findMPV = className =? "mpv"
    manageMPV = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.3
        width = 0.98
        height = 0.2
    spawnNyx = myTerminal ++ " " ++ "--title nyx --command nyx"
    findNyx = title =? "nyx"
    manageNyx = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.0
        width = 0.98
        height = 0.4
    spawnGnomeSystemMonitor = "gnome-system-monitor"
    findGnomeSystemMonitor = className =? "Gnome-system-monitor"
    manageGnomeSystemMonitor = defaultFloating
    spawnPulsemixer = myTerminal ++ " " ++ "--title pulsemixer --command pulsemixer"
    findPulsemixer = title =? "pulsemixer"
    managePulsemixer = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.0
        width = 0.98
        height = 0.4
    spawnLf = myTerminal ++ " " ++ "--title lf-scratch --command lf"
    findLf = title =? "lf-scratch"
    manageLf = customFloating $ StackSet.RationalRect x y width height
      where
        x = 0.01
        y = 0.0
        width = 0.98
        height = 0.4

--------------------------------------------------------------------------------

toggleFullscreen :: X ()
toggleFullscreen =
  withWindowSet $ \ws ->
    withFocused $ \w -> do
      let fullRect = StackSet.RationalRect 0 0 1 1
      let isFullFloat = w `M.lookup` StackSet.floating ws == Just fullRect
      windows $ if isFullFloat then StackSet.sink w else StackSet.float w fullRect

--------------------------------------------------------------------------------

type WSGroupId = String

--------------------------------------------------------------------------------

myModMask :: KeyMask
myModMask = mod4Mask -- Win key or Super_L

myKeys :: [((KeyMask, KeySym), X ())]
myKeys =  [
  ((myModMask, keySym), windows $ StackSet.greedyView workspace)
    | (keySym, workspace) <- myExtraWorkspaces
  ] ++ [
    ((myModMask .|. shiftMask, keySym), windows $ StackSet.shift workspace)
    | (keySym, workspace) <- myExtraWorkspaces
  ]


-- https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Util-EZConfig.html#g:2
myKeysP :: [([Char], X ())]
myKeysP = [

  -- kb_group layouts
  ("M-f", toggleFullscreen)

  -- KB_GROUP Apps
  , ("M-r", spawn "xmonad --recompile && xmonad --restart")
  , ("C-M-e", spawn "emacsclient --create-frame")
  -- , ("C-M-a", spawn "emacsclient --create-frame --socket-name=vanilla")
  , ("C-M-h", spawn "alacritty --command hx")
  -- , ("M-C-<Return>", spawn "emacsclient --create-frame --eval '(progn (sly) (delete-other-windows))' --socket-name=vanilla")
  , ("C-M-<Return>", spawn "emacsclient --create-frame --eval '(progn (eshell) (delete-other-windows))'")

  -- KB_GROUP Contexts
  , ("M-s", Contexts.listContextNames >>= Dmenu.dmenu >>= Contexts.createAndSwitchContext)
  , ("M-S-s", Contexts.listContextNames >>= Dmenu.dmenu >>= Contexts.deleteContext >> return ())

  -- KB_GROUP Windows
  , ("M-w", windowPrompt def { autoComplete = Just 500000 } Goto allWindows)
  , ("M-S-w", windowPrompt def Bring allWindows)
  , ("M-<Tab>", toggleFocus)

  -- KB_GROUP Scratchpads
  -- Toggle show/hide these programs.  They run on a hidden workspace.
  -- When you toggle them to show, it brings them to your current workspace.
  -- Toggle them to hide and it sends them back to hidden workspace (NSP).
  , ("M-'", namedScratchpadAction myScratchpads "emacs-float")
  , ("M-[", namedScratchpadAction myScratchpads "emacs-float-restclient")
  , ("M-]", namedScratchpadAction myScratchpads "emacs-float-calculator")
  , ("M-,", namedScratchpadAction myScratchpads "emacs-float-ielm")
  , ("M-.", namedScratchpadAction myScratchpads "emacs-float-eshell")
  , ("M-~", namedScratchpadAction myScratchpads "emacs-float-gpt")
  , ("M-q", namedScratchpadAction myScratchpads "emacs-float-gpt")
  , ("M-<Escape>", namedScratchpadAction myScratchpads "terminal-float")
  , ("M-<F1>", namedScratchpadAction myScratchpads "mpv")
  , ("M-<F2>", namedScratchpadAction myScratchpads "zotero")
  , ("M-<F3>", namedScratchpadAction myScratchpads "copyq")
  , ("M-<F4>", namedScratchpadAction myScratchpads "obs")
  , ("M-<F5>", namedScratchpadAction myScratchpads "lf")
  , ("M-<F6>", namedScratchpadAction myScratchpads "gcolor")
  -- , ("M-<F6>", namedScratchpadAction myScratchpads "gnome-clocks")
  , ("M-<F10>", namedScratchpadAction myScratchpads "nyx")
  , ("M-<F11>", namedScratchpadAction myScratchpads "gnome-system-monitor")
  , ("M-<F12>", namedScratchpadAction myScratchpads "pulsemixer")
  -- , ("<XF86MonBrightnessUp>", spawn "brightnessctl set +5")
  -- , ("<XF86MonBrightnessDown>", spawn "brightnessctl set 5-")

  -- KB_GROUP DynamicWorkspaceGroups
   -- , ("M-y a", promptWSGroupAdd myXPConfig "Name new group: ")
   -- , ("M-y g", promptTopicGroupView myTopicConfig myXPConfig "Go to group: ")
   -- , ("M-y f", promptWSGroupForget myXPConfig "Forget group: ")
  ]
  -- where
  -- myXPConfig = def
  --   {
  --     font = myFont
  --   , bgColor = base3 solarized
  --   , fgColor = base03 solarized
  --   , borderColor = magenta solarized
  --   , promptBorderWidth = 0
  --   }
  -- myTopicConfig :: TopicConfig
  -- myTopicConfig = def
  --   {
  --   }

-- https://www.reddit.com/r/xmonad/comments/pk7hf7/how_do_i_make_xmonad_to_move_only_floating_window/hc1oxn2/
-- myMouseBindings :: XConfig Layout -> M.Map (KeyMask, Button) (Window -> X ())
-- myMouseBindings (XConfig {XMonad.modMask = modMask}) = M.fromList
--     [ ((modMask, button3), \w -> do
--         floats <- gets $ floating . windowset
--         when (w `M.member` floats) $ do
--           focus w
--           mouseMoveWindow w
--           windows StackSet.shiftMaster)
--     ]
--------------------------------------------------------------------------------

-- myConfig :: XConfig
myConfig =
  def
    { modMask = myModMask
    , workspaces = myWorkspaces
    , layoutHook = myLayoutHook
    , manageHook = myManageHook <> namedScratchpadManageHook myScratchpads
    , startupHook = myStartupHook
    , borderWidth = 3
    , focusedBorderColor = magenta solarized
    , normalBorderColor = base2 solarized
    , terminal = myTerminal
    }
    `additionalKeys` myKeys
    `additionalKeysP` myKeysP

--------------------------------------------------------------------------------

main :: IO ()
main = do
  xmonad $ ewmh $ docks myConfig
