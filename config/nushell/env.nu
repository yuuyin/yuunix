# Nushell Environment Config File
#
# version = 0.79.0

def create_left_prompt [] {
    let path_segment = if (is-admin) {
        $"(ansi red_bold)($env.PWD)"
    } else {
        $"(ansi green_bold)($env.PWD)"
    }
    let user = whoami

    let datetime = $"(date now | date format '%Y-%m-%dT%H:%M:%S')"

    $"λ ($env.USER) @ (hostname) in ($path_segment) ($datetime)" | tr --delete "\n"
    #$"λ ($env.USER) @ (hostname) in ($path_segment)" | tr --delete "\n"
}

# Use nushell functions to define your right and left prompt
let-env PROMPT_COMMAND = {|| create_left_prompt }
let-env PROMPT_COMMAND_RIGHT = ""

# The prompt indicators are environmental variables that represent
# the state of the prompt
let-env PROMPT_INDICATOR = {|| "\nλ " }
let-env PROMPT_INDICATOR_VI_INSERT = {|| "\n: " }
let-env PROMPT_INDICATOR_VI_NORMAL = {|| "\nλ " }
let-env PROMPT_MULTILINE_INDICATOR = {|| "\n::: " }

# Specifies how environment variables are:
# - converted from a string to a value on Nushell startup (from_string)
# - converted from a value back to a string when running external commands (to_string)
# Note: The conversions happen *after* config.nu is loaded
let-env ENV_CONVERSIONS = {
  "PATH": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
  "Path": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
}

# Directories to search for scripts when calling source or use
#
# By default, <nushell-config-dir>/scripts is added
let-env NU_LIB_DIRS = ([
    ($nu.default-config-dir | path join 'scripts')
])

# Directories to search for plugin binaries when calling register
#
# By default, <nushell-config-dir>/plugins is added
let-env NU_PLUGIN_DIRS = [
    ($nu.default-config-dir | path join 'plugins')
]

# To add entries to PATH (on Windows you might use Path), you can use the following pattern:
# let-env PATH = ($env.PATH | split row (char esep) | prepend '/some/path')
