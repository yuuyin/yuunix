## shell.nix

# Copyright (C) 2022--2023  Yuu Yin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

## COMMENTARY

# Shell for bootstrapping flake-enabled nix and other tooling.

## CODE

{ (pkgs ?
  # IF pkgs is not defined, THEN instanciate nixpkgs from locked commit.
  let
    lock = (builtins.fromJSON (builtins.readFile ./flake.lock)).nodes.nixpkgs.locked;
    nixpkgs = fetchTarball {
      url = "https://github.com/nixos/nixpkgs/archive/${lock.rev}.tar.gz";
      sha256 = lock.narHash;
    };
  in import nixpkgs {
    overlays = [
    ];
  })
, ...
}:

pkgs.mkShell {
  NIX_CONFIG = "extra-experimental-features = nix-command flakes repl-flake";

  nativeBuildInputs = with pkgs; [
    nix
    home-manager
    git

    sops
    gnupg
    age
  ];
}
