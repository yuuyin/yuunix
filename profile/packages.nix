{ config
, lib
, pkgs
, ...
}:

{
  programs = {
    autojump.enable = true;
  };

  environment.systemPackages = with pkgs; [
    acpi
    linux-pam

    file
    lf
    fzf
    tree
    tracker
    xdg-user-dirs

    git
    gnupg
    openssh
  ];
}
