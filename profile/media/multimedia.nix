{ config
, lib
, pkgs
, pkgs-update
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = (with pkgs; [
      obs-studio

      # freac # FIXME: bad packaging can't FLAC
      mpv
      asciinema

      # FIXME: fails to build
      # davinci-resolve
      kdenlive
      shotcut
      simplescreenrecorder

      # natron # depends on unsupported python2
      blender
    ]) ++ (with pkgs-update; [
      ffmpeg-full
      yt-dlp
    ]);
  };
}
