{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = with pkgs; [
      gcolor2
      # https://github.com/flatpak/flatpak/issues/2446#issuecomment-448618648
      gcolor3

      # gmic
      # gmic-qt # fail
      # gmic-qt-krita  # removed as it's no longer supported upstream.
      gimp # depends on unsupported python2
      krita

      drawio
      inkscape

      rawtherapee

      maim

      imagemagick # for convert
      librsvg # for rsvg-convert
      svgcleaner
    ];
  };
}
