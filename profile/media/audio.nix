{ lib
, config
, pkgs
, pkgs-update
, ...
}:

{
  # Some audio servers like PulseAudio uses this.
  security.rtkit.enable = true;

  # Disable ALSA.
  sound.enable = false;

  # Disable PulseAudio server.
  hardware.pulseaudio.enable = false;

  services.pipewire = {
    enable = true;

    jack = {
      enable = true;
    };

    pulse = {
      enable = true;
    };

    alsa = {
      enable = false;
      support32Bit = true;
    };

    wireplumber = {
      enable = true;
    };
  };

  environment = {
    # Fix: audio source HDA Intel PCH ALC236 Analog: left channel to mono. Remap
    # the stereo input to a mono input. WHY: Built-in Lenovo Ideapad 320's
    # microphone has two channels but only one channel can provide a valid sound
    # signal. This causes issues (only static audio/noise) with microphone when
    # trying to use the microphone mainly with Web apps like meet.jit.si,
    # app.element.io, ...
    etc."pipewire/pipewire.conf.d/20-alsa_input.pci-0000_00_1f.3.analog-mono.conf" = {
      enable = false;

      source = pipewire/_/20-alsa_input.pci-0000_00_1f.3.analog-mono.conf;
    };

    systemPackages = with pkgs; [
      pulseaudio
      pulsemixer # for pactl
    ];
  };

  home-manager.users.yuu = {
    home.packages = (with pkgs; [
      # DAW
      ardour
      # audacity
      # lmms
      # stargate
      # bitwig-studio
      reaper

      # PLUGIN MANAGER
      carla

      # VIRTUAL MIDI
      vmpk
      mamba

      # SYNTH
      fluidsynth
      qsynth

      # JACK
      qjackctl

      # PLUGIN
      helm
      sfizz
      aether-lv2
      distrho # vitalium, ...
      cardinal

      # METADATA
      picard
      kid3
      # puddletag # levenshtein 0.18.1 is broken
    ]) ++ (with pkgs-update; [
      zrythm

      # PIPEWIRE
      helvum
    ]);
  };
}
