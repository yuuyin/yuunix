{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    services.dunst = {
      enable = false;
      configFile = ../../config/dunst/dunstrc;
    };
  };
}
