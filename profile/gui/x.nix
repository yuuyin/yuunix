{ config
, lib
, pkgs
, ...
}:

{
  services = {
    xserver = {
      enable = true;

      # Make sure /etc/X11/xkb is populated.
      # https://github.com/NixOS/nixpkgs/issues/19629#issuecomment-368051434
      exportConfiguration = true;

      # Configure keymap for X
      layout = "br";
      xkbOptions = "ctrl:nocaps";

      # Enable touchpad support (enabled by default in most desktopManager).
      libinput = {
        enable = true;
        touchpad = {
          disableWhileTyping = true;
          naturalScrolling = true;
          tapping = true;
        };
      };

      desktopManager = {
        # This hasn't the expected behavior since
        # services.xserver.enable adds xterm anyway.
        xterm.enable = false;

        # https://nixos.org/manual/nixos/unstable/release-notes.html#sec-release-22.05
        runXdgAutostartIfNone = true;
      };

      displayManager.startx.enable = true;
    };

    xbanish.enable = true;
  };

  home-manager.users.yuu = {
    programs = {
      # TODO: https://github.com/phillipberndt/autorandr
      autorandr = {
        enable = true;
      };
    };

    xresources = {
      properties = {
        "Xcursor.size" = "32";
        "Xcursor.theme" = "oreo_spark_orange_cursors";
        "Xft.antialias" = "true";
        "Xft.autohint" = "false";
        "Xft.dpi" = "96";
        "Xft.hinting" = "true";
        "Xft.hintstyle" = "hintslight";
        "Xft.lcdfilter" = "lcddefault";
        "Xft.rgba" = "rgb";

        # "Emacs*background" = "#ffffff";
        # "Emacs*foreground" = "#000000";
        # "Emacs*topShadowColor" = "#ffffff";
        # "Emacs*bottomShadowColor" = "#ffffff";
      };

      # FIXME: Enabling this makes polybar go dark.
      #   extraConfig = builtins.readFile (
      #     pkgs.fetchFromGitHub {
      #       owner = "solarized";
      #       repo = "xresources";
      #       rev = "025ceddbddf55f2eb4ab40b05889148aab9699fc";
      #       sha256 = "0lxv37gmh38y9d3l8nbnsm1mskcv10g3i83j0kac0a2qmypv1k9f";
      #     } + "/Xresources.light"
      #   );
    };

    # services = {
    #   random-background = {
    #     enable = true;
    #     enableXinerama = false;
    #   };
    # };

    systemd.user = {
      targets = {
        wallpaper = {
          Unit = {
            Description = "Wallpaper setting units";
            After = [
              "graphical-session.target"
            ];
            Wants = [
              "graphical-session.target"
            ];
          };
        };
      };

      services = {
        xwallpaper = {
          Unit = {
            Description = "Sets a wallpaper for X.org session.";
            Documentation = [
              "man:xwallpaper(1)"
            ];
            After = [
              "graphical-session.target"
            ];
            PartOf = [
              "graphical-session.target"
            ];
            Wants = [
              "graphical-session.target"
            ];
          };

          Service = {
            Type = "oneshot";
            ExecStart =
              "${pkgs.xwallpaper}/bin/xwallpaper --zoom /etc/nixos/asset/image/wallpaper-STScI-01G8H1NK4W8CJYHF2DDFD1W0DQ.1920x.png";
            TimeoutStopSec = "180";
            KillMode = "process";
            KillSignal = "SIGINT";
          };

          Install = {
            WantedBy = [
              "graphical-session.target"
            ];
          };
        };
      };

    #   timers = {
    #     xwallpaper = {
    #       Unit = {
    #         Description = "Sets wallpapers for X.org session.";
    #         Documentation = [
    #           "man:xwallpaper(1)"
    #         ];
    #       };
    #     };
    #   };
    };

    home.packages = with pkgs; [
      libva-utils
      xbrightness
      xclip
      xdo
      xdotool
      xorg.xcompmgr
      xorg.xfontsel
      xorg.xkill
      xorg.xrandr
      xsettingsd
      xwallpaper
      arandr
      lxrandr
    ];
  };
}
