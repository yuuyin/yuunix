{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    programs.eww = {
      enable = false;
      configDir = ../../config/eww;
    };

    home.packages = with pkgs; [
      eww
    ];
  };
}
