{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = [
      pkgs.rofi
    ];
  };
}
