{ config
, lib
, pkgs
, ... 
}:

{
  services = {
    udev = {
      # Allow users in the video group to change the brightness.
      extraRules = ''
        ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", GROUP="video", MODE="0664"
      '';
    };
  };

  home-manager.users.yuu.home.packages = with pkgs; [ 
    brightnessctl 
  ];
}
