{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    qt = {
      style.name = "kvantum";
    };

    # pam.sessionVariables = {
    #   QT_STYLE_OVERRIDE = "kvantum";
    # };

    home.packages = with pkgs; [
      breeze-qt5
      libsForQt5.qtstyleplugin-kvantum
      qt5ct
    ];
  };
}
