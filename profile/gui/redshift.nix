{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    services.redshift = {
      enable = true;
    };
  };
}
