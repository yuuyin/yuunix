{ config
, lib
, pkgs
, ...
}:

let
  blurlock = pkgs.writeShellApplication {
    name = "blurlock";
    text = ''
      get_resolution() {
        ${pkgs.xorg.xrandr}/bin/xrandr --query | \
          ${pkgs.gnused}/bin/sed -n 's/.*current[ ]\([0-9]*\) x \([0-9]*\),.*/\1x\2/p'
      }

      main() {
        resolution="$(get_resolution)"

        # Lock the screen.
        ${pkgs.imagemagick}/bin/import -silent -window root jpeg:- | \
          ${pkgs.imagemagick}/bin/convert - -scale 20% -blur 0x2.5 -resize 500% RGB:- | \
            ${pkgs.i3lock}/bin/i3lock --raw "$resolution":rgb -i /dev/stdin -e "$@"

        # sleep 1 adds a small delay to prevent possible race conditions with suspend
        # sleep 1

        exit 0
      }

      main "$@"
    '';
  };
in {
  home-manager.users.yuu = {
    home.packages = with pkgs; [
      blurlock
      i3lock
      # i3lock-color
      imagemagick
      xorg.xrandr
    ];

    services.screen-locker = {
      enable = true;
      inactiveInterval = 10;
      lockCmd = "\${pkgs.blurlock}/bin/blurlock";
    };
  };
}
