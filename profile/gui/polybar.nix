{ config
, lib
, pkgs
, ...
}:

# let
#   procps0 = pkgs.procps.overrideAttrs (oldAttrs: {
#     # greater than coreutils, which also provides "kill" and "uptime"
#     meta.priority = 0;
#   });
# in
{
  home-manager.users.yuu = {
    services.polybar = {
      enable = false;
      package = pkgs.polybarFull;
      script = ''
        #!/usr/bin/env sh
        # Launch polybar.
        #   https://wiki.archlinux.org/index.php/Polybar#Running_with_WM

        #==========================================================
        # ENVIRONMENT VARIABLES
        #==========================================================
        # NETWORK INTERFACES
        #   https://github.com/polybar/polybar/issues/339#issuecomment-447674287
        #   https://github.com/polybar/polybar/wiki/Formatting#format-tags-inside-polybar-config
        NETWORK_INTERFACE_WIRED="$(${pkgs.iproute2}/bin/ip address | grep ': enp.*:' | ${pkgs.gawk}/bin/gawk '{print $2}' | tr --delete ':' | head --lines=1)"
        NETWORK_INTERFACE_WIRELESS="$(${pkgs.iproute2}/bin/ip address | grep ': wl.*:' | ${pkgs.gawk}/bin/gawk '{print $2}' | tr --delete ':' | head --lines=1)"

        export NETWORK_INTERFACE_WIRED
        export NETWORK_INTERFACE_WIRELESS

        #==========================================================
        # RUN
        #==========================================================
        # Terminate running instances.
        pkill -12 polybar
        pkill --full pipewire-microphone.sh

        # Wait until the processes have been shut down.
        while pgrep --euid "$(id --user)" --exact polybar >/dev/null; do sleep 1; done

        # Launch Polybar, using default config location ~/.config/polybar/config
        # polybar $(whoami) --config=$(dirname $0)/config.ini &
        ${pkgs.polybar}/bin/polybar "$(whoami)" --config="~/.config/polybar/config.ini" &

        printf "%s\n" "Polybar launched."
      '';
    };

    # home.packages = with pkgs; [
    #   # for uptime --prety, and polybar system-uptime-pretty
    #   procps
    # ];
  };
}
