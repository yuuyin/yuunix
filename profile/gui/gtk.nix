{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    pam.sessionVariables = {
      GTK2_RC_FILES =
        "\${XDG_DATA_HOME}/themes/Yuu-Solarized-Light-Magenta/gtk-2.0/gtkrc";
    };

    home.packages = with pkgs; [
      gtk3 # for gtk3-widget-tools
      glib # for gsettings and yuu-reload-gtk-theme
    ];
  };
}
