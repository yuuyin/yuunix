{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = with pkgs; [
      papirus-icon-theme
      # https://github.com/ful1e5/fuchsia-cursor
    ];
  };
}
