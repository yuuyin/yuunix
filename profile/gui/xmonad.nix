{ config
, lib
, pkgs
, ...
}:

{
  services.xserver = {
    windowManager = {
      xmonad = {
        enable = true;
        enableContribAndExtras = true;
        extraPackages = haskellPackages: [
          haskellPackages.xmonad
          haskellPackages.xmonad-contrib
          haskellPackages.xmonad-extras
          haskellPackages.xmobar
        ];
      };
    };

    displayManager = {
      defaultSession = "none+xmonad"; # only effective for GDM, LightDM and SDDM
    };
  };

  home-manager.users.yuu = {
  #  xsession.windowManager.xmonad = {
  #    enable = true;
  #    enableContribAndExtras = true;
  #    extraPackages = haskellPackages: [
  #      haskellPackages.xmonad
  #      haskellPackages.xmonad-contrib
  #      haskellPackages.xmonad-extras
  #    ];
  #    # TODO: variable for referring to /etc/nixos/config
  #    # config = ../../config/xmonad/xmonad.hs;
  #  };

    home = {
      sessionVariables = {
        XMONAD_CONFIG_DIR = "\${XDG_CONFIG_HOME}/xmonad";
        # https://wiki.archlinux.org/title/Java#Gray_window,_applications_not_resizing_with_WM,_menus_immediately_closing
        _JAVA_AWT_WM_NONREPARENTING = "1";
      };

      packages = with pkgs; [
        xmonad-log # for ingetrating with polybar
        xmonad-with-packages
        dmenu # for Actions.Contexts
      ];
    };
  };
}
