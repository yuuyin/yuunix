{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = with pkgs; [
      # glyph/unicode
      font-awesome
      material-icons
      twemoji-color-font
      cm_unicode

      # monospace
      iosevka
      sgr-iosevka-fixed-bin

      # sans
      open-sans
      comic-relief

      # cjk
      source-han-sans
      noto-fonts-cjk-sans

      # manager / viewer
      font-manager # depends on glib-network > networkmanager > dnsmasq
      # fontmatrix
      gnome.gnome-characters
    ];
  };
}
