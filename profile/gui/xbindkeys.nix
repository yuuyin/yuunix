{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = [
      pkgs.xbindkeys
    ];

    # systemd.user.services.xbindkeys = {
    #   Unit = {
    #     Description = "xbindkeys, an X keybind manager";
    #     Documentation = [ "man:xbindkeys(5)" ];
    #     Wants = [ "graphical-session.target" ];
    #     Requires = [ "graphical-session.target" ];
    #     After = [ "graphical-session.target" ];
    #   };

    #   Service = {
    #     Type = "simple";
    #     ExecStart = "${pkgs.xbindkeys}/bin/xbindkeys";
    #     # JobTimeoutSec = "180";
    #     KillMode = "process";
    #     KillSignal = "SIGINT";
    #   };

    #   Install = { WantedBy = [ "graphical-session.target" ]; };
    # };
  };
}
