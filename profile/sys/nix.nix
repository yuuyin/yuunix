{ config
, lib
, pkgs
, inputs
, outputs
, ...
}:

{
  nixpkgs.config.permittedInsecurePackages = [
    "python-2.7.18.6"
  ];

  nix = {
    # https://github.com/cole-h/nixos-config/blob/1f171e5813a7b5e83651022639d818ec145e3f0e/flake.nix#L89-L96
    registry = {
      self.flake = inputs.self;

      nixpkgs =  {
        from = {
          id = "nixpkgs";
          type = "indirect";
        };

        # Pin flake registry to nixpkgs so that
        # it won't update in every system rebuild
        # and to be able to install imperatively with nix profile install.
        flake = inputs.nixpkgs;
      };
    };

    settings = {
      sandbox = true;
      auto-optimise-store = true;

      allowed-users = [
        "@wheel"
      ];
      trusted-users = [
        "@wheel"
      ];

      # substituters = [
      # ];
      # trusted-public-keys = [
      # ];
      require-sigs = true;
    };

    extraOptions = ''
      experimental-features = nix-command flakes

      # Protect nix-shell against garbage collection.
      keep-outputs = true
      keep-derivations = true
    '';
  };

  # gc = {
  #   automatic = true;
  #   dates = "weekly";
  #   options = "--delete-older-than 7d --max-freed $((64 * 1024**3))";
  # };

  # optimise = {
  #   automatic = true;
  #   dates = [
  #     "weekly"
  #   ];
  # };

  environment.systemPackages = with pkgs; [
    cachix
    nix-index
    nix-tree
    manix
    # cannot transmute between types of different sizes, or dependently-sized types
    # hydra-cli
    hydra-check
  ];
}
