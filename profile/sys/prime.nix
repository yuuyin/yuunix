{ config
, lib
, pkgs
, ...
}:

# https://nixos.wiki/wiki/Nvidia#Optimus

let
  # https://nixos.wiki/wiki/Nvidia#offload_mode
  nvidia-offload = pkgs.writeShellScriptBin "nvidia-offload" ''
    export __NV_PRIME_RENDER_OFFLOAD=1
    export __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0
    export __GLX_VENDOR_LIBRARY_NAME=nvidia
    export __VK_LAYER_NV_optimus=NVIDIA_only
    exec "$@"
    '';
in
{
  environment.systemPackages = with pkgs; [
    nvidia-offload
    glxinfo # glxgears
  ];

  services.xserver.videoDrivers = [
    "intel"
    "nvidia"
  ];

  hardware = {
    nvidia = {
      nvidiaPersistenced = false;

      prime = {
        offload.enable = true;

        # Bus ID of the GPUs. Find it using lspci, either under 3D or VGA.
        # https://github.com/NixOS/nixpkgs/pull/140539
        # https://github.com/NixOS/nixpkgs/commit/614b54d9b04092d6926afd49d64efe86272f7dd1
        # intelBusId = "PCI:0:2:0";
        # nvidiaBusId = "PCI:1:0:0";
        # https://discourse.nixos.org/t/struggling-with-nvidia-prime/13794/4
        intelBusId = "0@0:2:0";
        nvidiaBusId = "1@1:0:0";
      };
    };
  };
}
