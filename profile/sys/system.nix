{ config
, lib
, pkgs
, ...
}:

# trace: warning: Enabling both boot.enableContainers & virtualisation.containers on system.stateVersion < 22.05 is unsupported.

{
  # documentation = {
  #   enable = false;
  #   dev.enable = false;
  #   doc.enable = false;
  #   man.enable = false;
  #   nixos.enable = false;
  # };

  system = {
    # This value determines the NixOS release from which the default
    # settings for stateful data, like file locations and database versions
    # on your system were taken. It‘s perfectly fine and recommended to leave
    # this value at the release version of the first install of this system.
    # Before changing this value read the documentation for this option
    # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
    # https://nixos.org/manual/nixos/unstable/release-notes.html
    stateVersion = "22.11"; # Did you read the comment?

    autoUpgrade.channel = "https://nixos.org/channels/nixos-unstable";
  };
}
