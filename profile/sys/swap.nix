# https://discourse.nixos.org/t/how-do-i-set-up-a-swap-file/8323/7

{ config
, lib
, pkgs
, ...
}:

{
  # environment.systemPackages = with pkgs; [
  #   coreutils
  #   e2fsprogs
  #   btrfs-progs
  # ];

  services.logind.extraConfig = ''
    HandleHibernateKey=hibernate
  '';

  systemd.services = {
    systemd-logind = {
      serviceConfig = {
        # https://github.com/systemd/systemd/issues/15354#issuecomment-691695512
        # https://bbs.archlinux.org/viewtopic.php?pid=1928690#p1928690
        Environment = "SYSTEMD_BYPASS_HIBERNATION_MEMORY_CHECK=1";
      };
    };

    create-swapfile = {
      # truncate: failed to truncate '/swap/swapfile' at 0 bytes: Text file busy
      enable = false;
      serviceConfig.Type = "oneshot";
      wantedBy = [ "swap-swapfile.swap" ];
      script = ''
        ${pkgs.coreutils}/bin/truncate --size=0 /swap/swapfile
        ${pkgs.e2fsprogs}/bin/chattr +C /swap/swapfile
        ${pkgs.btrfs-progs}/bin/btrfs property set /swap/swapfile compression none
      '';
    };
  };
}
