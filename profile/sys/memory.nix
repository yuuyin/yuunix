{ config
, lib
, pkgs
, ...
}:

{
  services.earlyoom = {
    enable = true;
    enableNotifications = true; # -n
    freeMemThreshold = 5; # -m
    freeSwapThreshold = 60; # -s
  };

  environment.systemPackages = with pkgs; [
    earlyoom
  ];
}
