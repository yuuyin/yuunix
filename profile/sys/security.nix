{ config
, lib
, pkgs
, ...
}:

{
  security = {
    sudo = {
      enable = true;
    };
    
    apparmor = {
      enable = false;
    };
  };

  environment.systemPackages = with pkgs; [
    bubblewrap
    chkrootkit
  ];
}
