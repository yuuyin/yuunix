{ config
, lib
, pkgs
, home-manager
, ...
}:

{
  # system.activationScripts = {
  #     manpath =
  #     ''
  #     /bin/sh ${pkgs.man-db}/bin/manpath
  #     '';
  # };

  home-manager.users.yuu = {
    xdg.enable = true;
  };

  environment = {
    systemPackages = with pkgs; [
      mksh
    ];

    sessionVariables = rec {
      TZ_LIST = "America/Sao_Paulo;Europe/Moscow;Europe/Prague";

      # XDG
      XDG_CACHE_HOME = "$HOME/.cache";
      XDG_CONFIG_HOME = "$HOME/.config";
      XDG_BIN_HOME = "$HOME/.local/bin";
      XDG_DATA_HOME = "$HOME/.local/share";

      ANDROID_SDK_ROOT = "$HOME/Android/Sdk";

      # PATHS
      PATH = [
        "/etc/nixos/bin"
        "${XDG_CONFIG_HOME}/emacs/bin"
        "${XDG_BIN_HOME}"
        # "${ANDROID_SDK_ROOT}/emulator"
        # "${ANDROID_SDK_ROOT}/platform-tools"
      ];

      # MANPATH = config.system.activationScripts.manpath;

      DSSI_PATH = "$HOME/.dssi:$HOME/.nix-profile/lib/dssi:/run/current-system/sw/lib/dssi:/etc/profiles/per-user/yuu/lib/dssi";
      LADSPA_PATH = "$HOME/.ladspa:$HOME/.nix-profile/lib/ladspa:/run/current-system/sw/lib/ladspa:/etc/profiles/per-user/yuu/lib/ladspa";
      LV2_PATH = "$HOME/.lv2:$HOME/.nix-profile/lib/lv2:/run/current-system/sw/lib/lv2:/etc/profiles/per-user/yuu/lib/lv2";
      LXVST_PATH = "$HOME/.lxvst:$HOME/.nix-profile/lib/lxvst:/run/current-system/sw/lib/lxvst:/etc/profiles/per-user/yuu/lib/lxvst";
      VST_PATH = "$HOME/.vst:$HOME/.nix-profile/lib/vst:/run/current-system/sw/lib/vst:/etc/profiles/per-user/yuu/lib/vst";

      OPENER = "xdg-open";

      # APPS
      ## THEME
      QT_STYLE_OVERRIDE = "kvantum";
      GTK2_RC_FILES =
        "${XDG_DATA_HOME}/themes/Yuu-Solarized-Light-Magenta/gtk-2.0/gtkrc";

      ## DEFAULT APPS
      BROWSER = "firefox";
      EDITOR = "emacsclient";
      SUDO_EDITOR = "hx";
      TERM = "alacritty";
      TERMINAL = "alacritty";
      SHELL = "${pkgs.mksh}/bin/mksh";
      VISUAL = "emacsclient";

      ## JAVA
      ## https://wiki.archlinux.org/title/Java_Runtime_Environment_fonts#Overriding_the_automatically_picked_up_settings
      # _JAVA_OPTIONS = "-Dawt.useSystemAAFontSettings=on";

      ## APP CONFIG PATH
      ### XMONAD
      XMONAD_CONFIG_DIR = "\${XDG_CONFIG_HOME}/xmonad";

      ### EMACS
      EMACSDIR = "\${XDG_CONFIG_HOME}/emacs";
      DOOMDIR = "\${XDG_CONFIG_HOME}/doom";

      #### ORG
      ORG = "/mnt/pro/org";
      ORG_ROAM = "${ORG}";
      ORG_TMP = "${ORG}/tmp";
      ORG_NOTES_FILE = "${ORG}/notes.org";
      ORG_MIME = "${ORG}/mime";
      ORG_MIME_APPLICATION = "${ORG_MIME}/application";
      ORG_MIME_AUDIO = "${ORG_MIME}/audio";
      ORG_MIME_IMAGE = "${ORG_MIME}/image";
      ORG_MIME_TEXT = "${ORG_MIME}/text";
      ORG_MIME_VIDEO = "${ORG_MIME}/video";
      ORG_MIME_TMP = "${ORG_MIME}/tmp";
    };
  };
}
