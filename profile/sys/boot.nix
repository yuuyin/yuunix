{ config
, pkgs
, ...
}:

{
  boot = {
    loader = {
      systemd-boot = {
        enable = true;

        memtest86.enable = true;
      };

      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/efi";
      };
    };

    tmp = {
      useTmpfs = false;
      cleanOnBoot = true;
    };
  };
}
