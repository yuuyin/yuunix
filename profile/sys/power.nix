{ config
, lib
, pkgs
, ...
}:

{
  systemd = {
    # https://sawyershepherd.org/post/hibernating-to-an-encrypted-swapfile-on-btrfs-with-nixos/
    sleep.extraConfig = ''
      HibernateMode=shutdown
    '';
  };

  services = {
    # https://wiki.archlinux.org/title/TLP
    tlp = {
      enable = true;
      settings = {
        # Operation mode when no power supply can be detected: AC, BAT.
        TLP_DEFAULT_MODE = "BAT";

        # Operation mode select: 0=depend on power source, 1=always use TLP_DEFAULT_MODE
        TLP_PERSISTENT_DEFAULT = 1;
      };
    };
  };
}
