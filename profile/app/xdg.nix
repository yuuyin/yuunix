{ config
, lib
, pkgs
, ...
}:

{
  xdg = {
    portal = {
      enable = true;
      extraPortals = with pkgs; [
        xdg-desktop-portal-gtk
      ];
    };
  };

  environment.systemPackages = with pkgs; [
    xdg-user-dirs
  ];
}
