{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    services.kdeconnect = {
      enable = true;
      indicator = true;
    };
  };

  networking.firewall = {
    allowedTCPPortRanges = [
      {
        from = 1714;
        to = 1764;
      }
    ];
    allowedUDPPortRanges = [
      {
        from = 1714;
        to = 1764;
      }
    ];
  };
}
