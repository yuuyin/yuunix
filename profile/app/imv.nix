{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    xdg.desktopEntries = {
      imv = {
        name = "imv";
        genericName = "Image viewer";
        comment = "Fast freeimage-based Image Viewer";
        exec = "yuu-imv-dir %f";
        terminal = false;
        type = "Application";
        categories = [
          "Graphics"
          "2DGraphics"
          "Viewer"
        ];
        mimeType = [
          "image/bmp"
          "image/gif"
          "image/jpeg"
          "image/jpg"
          "image/pjpeg"
          "image/png"
          "image/tiff"
          "image/x-bmp"
          "image/x-pcx"
          "image/x-png"
          "image/x-portable-anymap"
          "image/x-portable-bitmap"
          "image/x-portable-graymap"
          "image/x-portable-pixmap"
          "image/x-tga"
          "image/x-xbitmap"
        ];
        icon = "multimedia-photo-viewer";
        # extraConfig = ''
        #   NoDisplay=true
        #   Keywords=photo;picture;
        # '';
      };
    };

    home.packages = with pkgs; [
      imv
    ];
  };
}
