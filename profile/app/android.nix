{ config
, lib
, pkgs
, ...
}:

{
  programs.adb.enable = true;

  home-manager.users.yuu = {
    home.packages = with pkgs; [
      # MTP
      android-file-transfer
      go-mtpfs
      mtpfs
    ];
  };
}
