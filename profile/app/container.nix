{ config
, lib
, pkgs
, ...
}:

{
  # for virtualisation.docker.enableNvidia
  hardware.opengl = {
    enable = true;
    driSupport32Bit = true;
  };

  virtualisation = {
    oci-containers = {
      backend = "podman";
      # containers = {
      #   "argos" = {
      #     autoStart = false;
      #     image = "docker.io/slavust/argos-translate:4e5d311bcb12";
      #   };
      # };
    };

    docker = {
      enable = true;
      enableNvidia = true; # for nvidia-docker
      storageDriver = "btrfs";

      # Mitigate "waiting for a stop job containerd" during shutdown.
      # https://discourse.nixos.org/t/docker-hanging-on-reboot/18270/5
      liveRestore = false;

      # socketActivation = false; # https://github.com/NixOS/nixpkgs/issues/14200
      # extraOptions = ''
      #   "data-root": "/mnt/sw/virt/oci"
      # '';
    };

    podman = {
      enable = true;

      extraPackages = with pkgs; [
        podman-compose
        podman-tui
        pods
      ];
    };
  };

  users.users.yuu.extraGroups = [
    "docker"
  ];

  # services = {
  #   kubernetes =
  # };

  environment.systemPackages = with pkgs; [
    arion
    docker-compose
    docker-machine
    kdash # kubernets monitoring
  ];
}
