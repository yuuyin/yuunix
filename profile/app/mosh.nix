{ config
, lib
, pkgs
, ...
}:

{
  programs = {
    mosh = {
      enable = true;
    };
  };
}
