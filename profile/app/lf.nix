{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    programs.lf = {
      enable = false;
    };

    home.packages = [
      pkgs.lf
    ];

    xdg.desktopEntries = {
      lf = {
        name = "lf";
        comment = "Launches the lf file manager";
        type = "Application";
        categories = [
          "ConsoleOnly"
          "System"
          "FileTools"
          "FileManager"
        ];
        exec = "alacritty -e lf";
        icon = "utilities-terminal";
        mimeType = [
          "inode/directory"
        ];
        terminal = false;
      };
    };
  };
}
