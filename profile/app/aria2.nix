{ config
, lib
, pkgs
, ... 
}:

{
  services = {
    aria2 = {
      enable = true;
      downloadDir = "/mnt/dump/aria2";
      extraArguments = ''
        --continue=true
        --check-integrity=true
        --max-connection-per-server=4
        --min-split-size=5M
        --enable-rpc=true
        --rpc-listen-all=true
      '';
    };
  };
}
