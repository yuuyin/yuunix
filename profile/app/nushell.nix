{ config
, lib
, pkgs-update
, ... 
}:

{
  home-manager.users.yuu = {
    programs.nushell = {
      package = pkgs-update.nushell;
      enable = true;
      configFile.source = ../../config/nushell/config.nu;
      envFile.source = ../../config/nushell/env.nu;  
    };
  };
}
