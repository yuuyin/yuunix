{ config
, lib
, pkgs
, inputs
, ...
}:

{
  home-manager.users.yuu = {
    programs.vscode = {
      enable = true;
      package = pkgs.vscodium;
    };
  };
}
