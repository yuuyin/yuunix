{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = with pkgs; [
      copyq
    ];

    systemd.user.services.copyq = {
      Unit = {
        Description = "CopyQ, a clipboard manager";
        Documentation = [
          "man:copyq(5)"
        ];
        Wants = [
          "graphical-session.target"
        ];
        Requires = [
          "graphical-session.target"
        ];
        After = [
          "graphical-session.target"
        ];
      };

      Service = {
        Type = "simple";
        ExecStart = "${pkgs.copyq}/bin/copyq";
        Environment = "\"QT_STYLE_OVERRIDE=kvantum\"";
        # JobTimeoutSec = "180";
        KillMode = "process";
        KillSignal = "SIGINT";
      };

      Install = {
        WantedBy = [
          "graphical-session.target"
        ];
      };
    };
  };
}
