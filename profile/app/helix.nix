{ config
, lib
, pkgs
, inputs
, ...
}:

{
  home-manager.users.yuu = {
    programs.helix = {
      enable = true;
      package = inputs.helix.packages.${pkgs.system}.default;
    };
  };

  environment.systemPackages = [
    inputs.helix.packages.${pkgs.system}.default
  ];
}
