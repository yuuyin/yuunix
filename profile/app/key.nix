{ config
, lib
, pkgs
, ...
}:

{
  services.logkeys = {
    enable = false;
    # device = "/dev/input/event0"; # li keyboard
    device = "/dev/input/event6"; # SZH usb keyboard
  };

  environment.systemPackages = with pkgs; [
    logkeys
    klavaro
    ktouch
  ];
}
