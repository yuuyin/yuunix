{ config
, lib
, pkgs
, ...
}:

{
  xdg.portal = {
    enable = true;
    extraPortals = [
      pkgs.xdg-desktop-portal-gtk
    ];
  };

  services.flatpak.enable = true;

  environment.systemPackages = with pkgs; [
    flatpak-builder
    libportal
  ];
}
