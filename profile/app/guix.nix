{ config
, lib
, inputs
, pkgs
, ...
}:

{
  nixpkgs.overlays = with inputs; [
    nix-overlay-guix.overlays.default
  ];

  imports = [
    inputs.nix-overlay-guix.nixosModules.guix
  ];

  services.guix = {
    enable = false;
    package = inputs.nix-overlay-guix.packages."x86_64-linux".guix;
  };

  environment.systemPackages = [
    inputs.nix-overlay-guix.packages."x86_64-linux".guix
  ];

  # environment.sessionVariables = rec {
  #   # GUIX_PROFILE = "$HOME/.config/guix/current";
  #   # GUIX_LOCPATH = "${GUIX_PROFILE}/lib/locale";

  #   GUIX_PROFILE = "$HOME/.guix-profile";
  #   GUIX_LOCPATH = "${GUIX_PROFILE}/lib/locale";
  # };

  # https://reddit.com/r/NixOS/comments/kn3kts
  # https://wiki.archlinux.org/title/Systemd-nspawn
  # systemd.nspawn = {
  #   gnu-guix-system = {
  #     enable = false;
  #     # [Exec]
  #     execConfig = {
  #       Boot = true;
  #       # Capability = "all";
  #     };

  #     # [Files]
  #     filesConfig = {

  #     };

  #     # [Network]
  #     networkConfig = {

  #     };
  #   };
  # };

}
