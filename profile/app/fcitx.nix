{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    i18n.inputMethod = {
      enabled = "fcitx5";
      fcitx5.addons = with pkgs; [
        fcitx5-configtool
        fcitx5-gtk
        fcitx5-lua
        fcitx5-m17n
        fcitx5-mozc
        libsForQt5.fcitx5-qt
      ];
    };

    # pam.sessionVariables = {
    #   GTK_IM_MODULE = "fcitx";
    #   QT_IM_MODULE = "fcitx";
    #   XMODIFIERS = "@im=fcitx";
    #   SDL_IM_MODULE = "fcitx";
    # };

    # home.sessionVariables = {
    #   GTK_IM_MODULE = "fcitx";
    #   QT_IM_MODULE = "fcitx";
    #   XMODIFIERS = "@im=fcitx";
    #   SDL_IM_MODULE = "fcitx";
    # };
  };

  environment.variables = {
    GTK_IM_MODULE = "fcitx";
    QT_IM_MODULE = "fcitx";
    XMODIFIERS = "@im=fcitx";
    SDL_IM_MODULE = "fcitx";
  };
}
