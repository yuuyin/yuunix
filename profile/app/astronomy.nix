{ config
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = with pkgs; [
      # openspace
      stellarium
      celestia
      # gaiasky
      # kstars
      # cosmonium
    ];
  };
}
