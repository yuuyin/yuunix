{ config
, lib
, pkgs
, ...
}:

{
  programs.zsh = {
    enable = true;
    autosuggestions.enable = true;
    enableCompletion = true;
    syntaxHighlighting.enable = true;
    ohMyZsh = {
      enable = true;
      theme = "ys";
    };
  };

  home-manager.users.yuu = {
    programs.zsh = {
      enable = true;
      enableAutosuggestions = true;
      enableCompletion = true;
      enableSyntaxHighlighting = true;
      enableVteIntegration = true;
      autocd = true;
      defaultKeymap = "emacs";
      # dotDir = "${config.xdg.configHome}/zsh;
      # history.path = "${config.xdg.dataHome}/zsh/zsh_history";
      oh-my-zsh = {
        enable = true;
        theme = "ys";
      };
    };
  };
}
