{ config
, lib
, pkgs
, pkgs-update
, ...
}:

{
  home-manager.users.yuu = {
    programs.firefox = {
      enable = false;
    };

    home.packages = with pkgs-update; [
      firefox
    ];

    xdg.desktopEntries = {
      firefox-profile-manager = {
        name = "Firefox (Profile Manager)";
        genericName = "Web Browser (Profile Manager)";
        comment = "Browse the World Wide Web (Profile Manager)";
        exec = "firefox --ProfileManager %u";
        terminal = false;
        startupNotify = true;
        type = "Application";
        categories = [
          "Network"
          "WebBrowser"
        ];
        mimeType = [
          "application/json"
          "application/pdf"
          "application/x-xpinstall"
          "application/xhtml+xml"
          "text/html"
          "text/xml"
          "x-scheme-handler/http"
          "x-scheme-handler/https"
        ];
        icon = "firefox";
        # extraConfig = ''
        #   Keywords=Internet;WWW;Browser;Web;Explorer
        #   X-MultipleArgs=false
        #   StartupWMClass=firefox
        # '';
      };
    };
  };
}
