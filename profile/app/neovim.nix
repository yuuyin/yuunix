{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    programs.neovim = {
      enable = true;
      extraPackages = with pkgs; [
        neovide
      ];
    };
  };
}
