{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    programs.tmux = {
      enable = true;
      aggressiveResize = true; # helps "grouped sessions" and multi-monitor setup
      clock24 = true;
      escapeTime = 0; # address vim mode switching delay
      historyLimit = 50000;
      keyMode = "emacs";
      shortcut = "x";
      terminal = "tmux-256color";
      extraConfig = ''
        set -g status-style bg='#D33682'

        # tmux messages are displayed for 4 seconds
        set -g display-time 4000

        # refresh 'status-left' and 'status-right' more often
        set -g status-interval 5

        # focus events enabled for terminals that support them
        set -g focus-events on

        # bindings
        bind 3 %
      '';
    };
  };
}
