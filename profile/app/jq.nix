{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    programs.jq = {
      enable = true;
    };
  };
}
