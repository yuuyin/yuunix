{ config
, lib
, pkgs
, ...
}:

{
  virtualisation = {
    libvirtd = {
      enable = true;
      extraConfig = ''
      '';
      extraOptions = [

      ];
      qemu = {
        # For UEFI
        ovmf = {
          enable = true;
        };
      };
    };
  };

  programs.dconf.enable = true;

  environment.systemPackages = with pkgs; [
    virt-manager
    libguestfs
  ];
}
