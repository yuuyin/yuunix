{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = with pkgs; [
      dhall
      dhall-json
      haskellPackages.dhall-yaml
      dhall-nix
      dhall-nixpkgs
      dhall-lsp-server
    ];
  };
}
