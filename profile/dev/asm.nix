{ config
, lib
, pkgs
, ...
}:

{

  environment.systemPackages = with pkgs; [
    # style
    asmfmt

    # assembler
    nasm
    yasm
    llvm # llvm-as

    # Debugger
    gdb
    lldb

    # Graphical debugger
    gede
    kdbg
  ];

}
