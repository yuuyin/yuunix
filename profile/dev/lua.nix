{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = with pkgs; [
      lua
      sumneko-lua-language-server
    ];
  };
}
