{ config, lib, pkgs, inputs, ... }:

{
  environment.systemPackages = with pkgs; [
    clojure
    clojure-lsp
    leiningen
    clj-kondo

    # fast? interpreter for scripting
    babashka
  ];
}
