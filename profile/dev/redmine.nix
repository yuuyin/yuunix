{ config
, lib
, pkgs
, ...
}:

{
  services.redmine = {
    enable = true;

    database = {
      type = "postgresql";
    };
  };
}
