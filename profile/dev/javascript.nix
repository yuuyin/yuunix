{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    tree-sitter-grammars.tree-sitter-javascript
    tree-sitter-grammars.tree-sitter-tsx
    tree-sitter-grammars.tree-sitter-typescript
  ];

  home-manager.users.yuu = {
    home.packages = with pkgs; [
      deno
      nodejs
      nodePackages.js-beautify

      nodePackages.typescript-language-server
      nodePackages.vscode-json-languageserver

      # nodePackages.node2nix
      # npmlock2nix

      react-native-debugger

      yarn-berry
    ];
  };
}
