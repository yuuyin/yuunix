{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    shen-sources
    shen-sbcl
  ];
}
