{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    asciidoctor
    asciidoctor-with-extensions
    mdbook
    yaml-language-server
  ];
}
