{ config
, lib
, pkgs-update
, ...
}:

{
  environment.systemPackages = (with pkgs-update; [
    python311
    pipenv
    poetry
  ]) ++ (with pkgs-update.python311.pkgs; [
    # pyls fork
    python-lsp-server
  ]);
}
