{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    clang
    clang-tools
    lldb

    gcc
    gdb

    cmake

    automake
    autoconf

    tree-sitter-grammars.tree-sitter-c
  ];
}
