{ config
, lib
, pkgs
, ...
}:

{
  programs.java = {
    enable = true;
    package = pkgs.openjdk;
  };

  environment.systemPackages = with pkgs; [
    eclipses.eclipse-java
    eclipses.eclipse-platform

    ant
    maven3
    gradle

    # openjdk8
    spring-boot-cli

    jdt-language-server

    tree-sitter-grammars.tree-sitter-java
  ];
}
