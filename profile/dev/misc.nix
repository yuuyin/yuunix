{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    libtool
    strace

    # tree-sitter for various languages.
    tree-sitter-grammars.tree-sitter-dockerfile
    tree-sitter-grammars.tree-sitter-elisp
    tree-sitter-grammars.tree-sitter-graphql
    tree-sitter-grammars.tree-sitter-json5
    tree-sitter-grammars.tree-sitter-ql
    tree-sitter-grammars.tree-sitter-r
    tree-sitter-grammars.tree-sitter-regex
    tree-sitter-grammars.tree-sitter-scheme
    tree-sitter-grammars.tree-sitter-sql
    tree-sitter-grammars.tree-sitter-toml
    tree-sitter-grammars.tree-sitter-yaml
  ];

  home-manager.users.yuu = {
    home.packages = with pkgs; [
      # strictdoc
      ruby
      rubyPackages.solargraph

      # android
      nodePackages.react-native-cli

      # src-cli
    ];
  };
}
