{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    mono
    dotnet-sdk
    # fsharp # deprecated in favor of dotnet-sdk
  ];
}
