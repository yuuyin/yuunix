{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    (haskellPackages.ghcWithPackages (pkgs: with pkgs; [
      ghc
      xmonad
      xmonad-contrib
      simplex-method
    ]))

    tree-sitter-grammars.tree-sitter-haskell
  ];

  home-manager.users.yuu = {
    home.packages = with pkgs; [
      ghcid

      haskellPackages.cabal-install
      haskellPackages.stack

      haskellPackages.hasktags
      haskellPackages.hoogle
      haskellPackages.hpack
      haskellPackages.stylish-haskell

      # Profiling parallel Haskell programs.
      # haskellPackages.threadscope # FIXME: marked as broken

      haskell-language-server
    ];
  };
}
