{ config
, lib
, inputs
, pkgs
, pkgs-update
, ...
}:

{
  nixpkgs.overlays = with inputs; [
    # dream2nix.overlays.default
    poetry2nix.overlay
    nixpkgs-hammering.overlays.default
  ];

  home-manager.users.yuu = {
    # https://github.com/direnv/direnv/wiki/Nix
    programs.direnv = {
      enable = true;
      enableNushellIntegration = true;
      enableZshIntegration = false;
      enableFishIntegration = false;
      nix-direnv.enable = true; # has flakes support
    };
  };

  environment.systemPackages = (with pkgs; [
    # LANGUAGE SERVER PROTOCOL
    nil
    # rnix-lsp

    # TREE-SITTER
    tree-sitter-grammars.tree-sitter-nix
    tree-sitter-grammars.tree-sitter-nickel

    # FORMATTING
    alejandra
    nixfmt
    nixpkgs-fmt

    # LINTER/SUGGESTION
    statix

    # 2NIX
    # mvn2nix
    # poetry2nix

    # devshell

    # DOCUMENTATION
    nix-tour

    # REVIEW
    nixpkgs-review
    nixpkgs-hammering

    # CACHE
    # nix-serve
    # nar-serve
  ]) ++ (with pkgs-update; [
    # AUTO
    nix-template
    # nix-init
    nurl
  ]) ++ [
    inputs.nix-init.packages."x86_64-linux".default
  ];
}
