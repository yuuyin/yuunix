{ config
, lib
, pkgs
, ...
}:

{
  services.postgresql = {
    enable = true;
    package = pkgs.postgresql;
  };

  environment.systemPackages = with pkgs; [
    postgresql
    postgresql_14
  ];

  home-manager.users.yuu = {
    home.packages = with pkgs; [
      # pgadmin # openssl-1.0.2u in openssl/default.nix:165 is marked as insecure
      pgmodeler
    ];
  };
}
