{ config
, lib
, pkgs
, ...
}:

{

  environment.systemPackages = with pkgs; [
    elmPackages.elm
    elmPackages.elm-language-server
  ];

}
