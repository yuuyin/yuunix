{ config
, lib
, pkgs
, inputs
, ...
}:

{
  # nixpkgs.overlays = with inputs; [
  #   cargo2nix.overlays.default
  # ];

  environment.systemPackages = with pkgs; [
    cargo
    rustc
    pkg-config # for cargo build

    glib

    tree-sitter-grammars.tree-sitter-rust
  ];

  home-manager.users.yuu = {
    home.packages = with pkgs; [
      rustfmt # code formatter
      rust-analyzer # lsp
      # FIXME: failed test
      # evcxr # repl
      cargo-watch # monitors changes for auto cargo check
      clippy # linter
      lld # faster linker
      cargo-tarpaulin # code coverage
      cargo-audit # security vulnerabilities
    ];
  };
}
