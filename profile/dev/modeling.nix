{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    eclipses.eclipse-modeling
    graphviz
    # gaphor
    plantuml
    umlet
  ];
}
