{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    scala

    sbt # build
    ammonite # enhanced repl
    scalafmt # formatting

    coursier
    # metals # issue wait until https://github.com/NixOS/nixpkgs/pull/144255 hits nixos-unstable
  ];
}
