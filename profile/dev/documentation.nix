{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = with pkgs; [
      devdocs-desktop
      # gnome.devhelp
      zeal
    ];

    systemd.user.services.zeal = {
      Unit = {
        Description = "Zeal, offline documentation browser";
        Wants = [ "graphical-session.target" ];
        Requires = [ "graphical-session.target" ];
        After = [ "graphical-session.target" ];
      };

      Service = {
        Type = "simple";
        ExecStart = "${pkgs.zeal}/bin/zeal";
        Environment = "\"QT_STYLE_OVERRIDE=kvantum\"";
        # JobTimeoutSec = "180";
        KillMode = "process";
        KillSignal = "SIGINT";
      };

      Install = {
        WantedBy = [
          "graphical-session.target"
        ];
      };
    };
  };
}
