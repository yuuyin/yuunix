{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = (with pkgs; [
      R
      tree-sitter-grammars.tree-sitter-r
    ]) ++ (with pkgs.rPackages; [
      languageserver
    ]);
  };
}