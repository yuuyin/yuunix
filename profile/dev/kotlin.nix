{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    tree-sitter-grammars.tree-sitter-kotlin
  ];

  home-manager.users.yuu = {
    home.packages = with pkgs; [
      kotlin
      kotlin-language-server
      kotlin-native
    ];
  };
}
