{ config
, lib
, pkgs
, ...
}:

{

  environment.systemPackages = with pkgs; [
    guile_3_0
  ];

}
