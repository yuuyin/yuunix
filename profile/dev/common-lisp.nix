{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    sbcl
  ] ++ (with lispPackages; [
    # https://lispcookbook.github.io/cl-cookbook/files.html#using-libraries
    alexandria
    osicat
  ]);
}
