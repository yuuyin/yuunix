{ config
, lib
, pkgs
, ...
}:

{
  imports = [
    ./mysql.nix
    ./postgresql.nix
    ./sqlite.nix
  ];

  environment.systemPackages = with pkgs; [
    # langauge server
    sqls

    # dbms ide
    dbeaver

    # modeling
    brmodelo

    # format
    pgformatter
    python3Packages.sqlparse

    # NoSQL
    rocksdb
    rocksdb_lite
  ];
}
