{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    protobuf # for protoc
  ];
}
