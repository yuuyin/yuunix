{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    mercurial
  ];

  home-manager.users.yuu = {
    home.file.".hgrc".text = ''
      [ui]
      username = Yuu Yin

      [extensions]
      progress =
    '';
  };
}
