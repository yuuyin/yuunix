{ config
, lib
, pkgs
, ...
}:

{
  # networking.firewall.allowedTCPPorts = [
  #   jupyterPort
  # ];

  users.users = {
    jupyter = {
      group = "jupyter";
      isNormalUser = false;
    };

    yuu.extraGroups = [
      "jupyter"
    ];
  };

  services.jupyter = {
    enable = true;

    package = pkgs.python3.pkgs.jupyterlab;
    command = "jupyter-lab";
    port = 54829;
    password = "''";

    kernels = {
      # let
      #   env = ;
      # in {
      #   julia = (
      #     displayName = "Julia";
      #   );
      # }

      python3 = (
        let
          env = (pkgs.python3.withPackages (pythonPackages: with pythonPackages; [
            ipykernel
            pandas
            scikit-learn
          ]));
        in {
          displayName = "Python 3 for machine learning";

          argv = [
            "${env.interpreter}"
            "-m"
            "ipykernel_launcher"
            "-f"
            "{connection_file}"
          ];

          language = "python";

          # error: is not of type `null or path'.
          # logo32 = "${env.sitePackages}/ipykernel/resources/logo-32x32.png";
          # logo64 = "${env.sitePackages}/ipykernel/resources/logo-64x64.png";
        }
      );
    };
  };
}
