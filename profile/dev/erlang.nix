{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    erlang
    erlang-ls
  ];
}
