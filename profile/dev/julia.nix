{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    # julia # https://github.com/NixOS/nixpkgs/issues/125822
    julia-bin
    tree-sitter-grammars.tree-sitter-julia
  ];
}
