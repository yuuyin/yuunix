{ config
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    # nushell
    powershell

    shellcheck
    shfmt
  ];
}
