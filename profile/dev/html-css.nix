{ config
, lib
, pkgs
, ... 
}:

{
  environment.systemPackages = with pkgs; [
    html-tidy
    hugo
    nodePackages.stylelint # for css style lint
    tree-sitter-grammars.tree-sitter-html
    tree-sitter-grammars.tree-sitter-css
  ];
}