{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    nodePackages.snyk
  ];
}
