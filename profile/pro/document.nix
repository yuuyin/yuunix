{ config
, lib
, pkgs
, pkgs-update
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = (with pkgs; [
      # PDF EDITOR
      masterpdfeditor4
      pdfarranger
      pdfslicer
      pdftk
      ghostscript
      qpdf

      # READER, MARKER
      okular
      xournalpp
      zathura

      # EPUB
      epubcheck
      sigil # epub editor

      # luatex:
      # Package minted Error:
      # You must have `pygmentize' installed to use this package
      python3Packages.pygments

      # MINDMAP
      # freemind
      freeplane
      minder

      # libreoffice-fresh
    ]) ++ (with pkgs-update; [
       libgen-cli
       papis
       calibre
    ]);
  };
}
