{ config
, lib
, pkgs
, ...
}:

let
  texlive-mod = pkgs.texlive.combine {
    inherit (pkgs.texlive)
      latex-bin biblatex capt-of enumitem latexmk metafont nopageno scheme-basic
      titlesec titling ulem wrapfig;
  };
in {
  environment.systemPackages = with pkgs; [
    texlive.combined.scheme-full
    # texlive-mod

    tree-sitter-grammars.tree-sitter-latex
    tree-sitter-grammars.tree-sitter-bibtex
  ];
}
