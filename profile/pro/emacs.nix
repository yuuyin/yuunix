{ config
, lib
, pkgs
, pkgs-master
, inputs
, ...
}:

let
  emacs-application-framework = {
    pkgs = with pkgs.python3.pkgs; [
      # EMACS APPLICATION FRAMEWORK (EAF)
      # TODO: needs *qt6
      # https://github.com/emacs-eaf/emacs-application-framework/blob/master/dependencies.json
      pyqt sip
      pyqtwebengine
      epc lxml

      # eaf-file-browser
      qrcode

      # eaf-browser
      pysocks

      # eaf-pdf-viewer
      pymupdf

      # eaf-file-manager
      pypinyin

      # eaf-system-monitor
      psutil

      # eaf-markdown-previewer
      retry
      markdown
    ];
  };
in {
  nixpkgs.overlays = [
    inputs.emacs-overlay.overlays.default
  ];

  home-manager.users.yuu = {
    programs.emacs = {
      enable = true;

      # extraOptions = [
      #   "--init-directory="
      #   "/home/yuu/emacs"
      # ];

      package = pkgs.emacsGit.override {
        treeSitterPlugins = with pkgs.tree-sitter-grammars; [
          tree-sitter-bash
          tree-sitter-c
          tree-sitter-c-sharp
          tree-sitter-cmake
          tree-sitter-cpp
          tree-sitter-css
          tree-sitter-dockerfile
          tree-sitter-go
          tree-sitter-gomod
          tree-sitter-html
          tree-sitter-java
          tree-sitter-javascript
          tree-sitter-json
          tree-sitter-python
          tree-sitter-ruby
          tree-sitter-rust
          tree-sitter-toml
          tree-sitter-tsx
          tree-sitter-typescript
          tree-sitter-yaml

          tree-sitter-julia
          tree-sitter-r
        ];
      };

      extraPackages = epkgs: with epkgs; [
        emacsql-sqlite # for org-roam, Doom Emacs' forge module
        emojify
        emojify-logos
        pdf-tools
        vterm
        jinx
      ];
    };

    services.emacs = {
      enable = true;
    };

    home.packages = with pkgs; [
      # org-re-reveal
      nodePackages."reveal.js"
      documentation-highlighter # highlight.js

      # org-roam-bibitex
      anystyle-cli

      imagemagick

      ccls
      editorconfig-core-c

      sqlite

      # EMAIL
      notmuch
      # mu
      # isync
      # afew
      # pass

      # LANGUAGE SERVER PROTOCOL (LSP)
      nodePackages.javascript-typescript-langserver
    ];
  };

  # System-wide because I need to use with other users e.g. sudo/root.
  environment.systemPackages = with pkgs; [
    enchant.dev # for jinx
    pkg-config # for jinx

    (aspellWithDicts (dicts: with dicts; [
      en
      en-computers
      en-science
      pt_BR
    ]))

    languagetool

    fd
    gnutls
    ripgrep

    unzip # for nov.el
    zstd

    libvterm # for vterm

    # https://lists.gnu.org/archive/html/bug-gnu-emacs/2019-12/msg00060.html
    # https://www.baeldung.com/linux/ld_preload-trick-what-is
    # https://github.com/jemalloc/jemalloc/wiki/Getting-Started
    # jemalloc

    # TREE SITTER
    # NOTE: home-manager-path> error: collision between
    # `tree-sitter-haskell-grammar-0.20.7/parser'
    # and `tree-sitter-c-grammar-0.20.7/parser'.
    tree-sitter-grammars.tree-sitter-bibtex
    tree-sitter-grammars.tree-sitter-c
    tree-sitter-grammars.tree-sitter-css
    tree-sitter-grammars.tree-sitter-dockerfile
    tree-sitter-grammars.tree-sitter-elisp
    tree-sitter-grammars.tree-sitter-graphql
    tree-sitter-grammars.tree-sitter-haskell
    tree-sitter-grammars.tree-sitter-janet-simple
    tree-sitter-grammars.tree-sitter-javascript
    tree-sitter-grammars.tree-sitter-json5
    tree-sitter-grammars.tree-sitter-kotlin
    tree-sitter-grammars.tree-sitter-html
    tree-sitter-grammars.tree-sitter-latex
    tree-sitter-grammars.tree-sitter-nickel
    tree-sitter-grammars.tree-sitter-nix
    tree-sitter-grammars.tree-sitter-ql
    tree-sitter-grammars.tree-sitter-r
    tree-sitter-grammars.tree-sitter-regex
    tree-sitter-grammars.tree-sitter-rust
    tree-sitter-grammars.tree-sitter-scheme
    tree-sitter-grammars.tree-sitter-sql
    tree-sitter-grammars.tree-sitter-toml
    tree-sitter-grammars.tree-sitter-tsx
    tree-sitter-grammars.tree-sitter-typescript
    tree-sitter-grammars.tree-sitter-yaml
  ];
}
