{ config
, lib
, pkgs
, ... 
}:

{
  home-manager.users.yuu = {
    home.packages = with pkgs; [
      ocrmypdf
      tesseract5
    ];
  };
}
