{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    biber
    zotero
  ];
}
