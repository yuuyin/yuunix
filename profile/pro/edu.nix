{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    anki-bin
  ];
}
