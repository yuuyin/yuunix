{ config
, lib
, pkgs
, ...
}:

{
  environment.systemPackages = with pkgs; [
    skrooge
    kmymoney
  ];
}
