{ config
, lib
, pkgs
, ...
}:

{
  home-manager.users.yuu = {
    home.packages = with pkgs; [
      typst
      typst-fmt
      typst-lsp
    ];
  };
}
