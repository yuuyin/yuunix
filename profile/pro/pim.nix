{ config
, lib
, pkgs-update
, ... 
}:

{
  environment.systemPackages = with pkgs-update; [
    logseq
  ];
}
