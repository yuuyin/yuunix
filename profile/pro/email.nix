{ config
, lib
, pkgs
, ...
}:

{
  programs = {
    geary.enable = false;
  };

  home-manager.users.yuu = {
    programs = {
      notmuch = {
        enable = false;
      };

      mbsync = {
        enable = true;
      };
    };

    home.packages = with pkgs; [
      # IMAP
      isync

      # POP3
      mpop
      # getmail6
      # fdm
      # fetchmail

      mu
      pass
    ];
  };
}
