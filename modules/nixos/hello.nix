{ config
, lib
, pkgs
}:

with lib;

let
  cfg = config.services.hello;
in
{
  options.services.hello = {
    enable = mkEnableOption "hello service";

    greeter = mkOption {
      type = types.str;
      default = "Yuu Yin";
      description = ''
        A service that outputs a string.
      '';
      example = "Yuu Yin";
    };

    package = mkOption {
      type = types.package;
      default = pkgs.hello;
      defaultText = literalExpression "pkgs.hello";
      description = ''
        hello derivation to use.
      '';
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [
      cfg.package
    ];

    systemd.services.hello = {
      wantedBy = [
        "multi-user.target"
      ];
      serviceConfig.ExecStart =
        "${pkgs.hello}/bin/hello --greeting 'Hello, ${escapeShellArg cfg.greeter}!'";
    };
  };
}
