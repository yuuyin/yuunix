{
  # Add my custom packages.
  additions = final: prev: import ../packages {
    pkgs = final;
    pkgs-update = final;
  };

  # Modify existing packages.
  modifications = overlayFinal: overlayPrev: {
    fritzing = overlayPrev.fritzing.overrideAttrs (oldAttrs: {
      pname = "fritzing";
      version = "unstable-2022-07-01";

      src = overlayPrev.fetchFromGitHub {
        owner = "fritzing";
        repo = "fritzing-app";
        rev = "40d23c29b0463d5c968c3c4b34ed5ffc05c5a258";
        sha256 = "sha256-smvfuxQWF/LMFFXHOKb3zUZsEet/XoiaxXOR5QMaYzw=";
      };

      postInstall =  ''
        install -Dm644 ${overlayPrev.libngspice}/lib/libngspice.so \
          $out/share/Fritzing/Fritzing/libngspice.so
      '';
    });

    podman-compose = overlayPrev.podman-compose.overrideAttrs (oldAttrs: {
      pname = "podman-compose";
      version = "unstable-2022-10-18";
      src = overlayPrev.fetchFromGitHub {
        owner = "containers";
        repo = "podman-compose";
        rev = "801faea30b1d320821f098861d716ffe7676b224";
        sha256 = "sha256-AFirQQ9USgY+lq2wH6L4rV5hAVytKWszDBRC2BFrw8Y=";
      };
    });

    sgr-iosevka-fixed-bin = overlayPrev.iosevka-bin.overrideAttrs (oldAttrs: {
      variant = "sgr-iosevka-fixed";
    });

    #   propagatedBuildInputs = oldAttrs.propagatedBuildInputs ++ [ pkgs.ntfs3g ];
    # });
  };
}
